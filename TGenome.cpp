/*
 * TGenome.cpp
 *
 *  Created on: Mar 15, 2015
 *      Author: wegmannd
 */


#include "TGenome.h"

//-------------------------------------------------------
//TGenome
//-------------------------------------------------------
TGenome::TGenome(TLog* Logfile, TParameters & params){
	logfile = Logfile;
	initializeRandomGenerator(params);

	//read window parameters
	if(!params.parameterExists("window") && params.parameterExists("windows")) logfile->warning("Argument 'windows' specified, but unknown. Did you mean 'window'?");
	windowSize = params.getParameterDoubleWithDefault("window", 10000);
	//if(windowSize < 1000) throw "Window size should be at least 1Kb!";
	refAndMask = new TReferenceAndMask(logfile, params, windowSize);

	//read BAM file names
	std::vector<std::string> bamFileNames;
	params.fillParameterIntoVector("bamList", bamFileNames, ',');
	numBamFiles = bamFileNames.size();
	if(numBamFiles < 1) throw "At least one bam file must be specified with argument bamList!";
	std::vector<std::string>::iterator itAfter;
	for(std::vector<std::string>::iterator it=bamFileNames.begin(); it!=bamFileNames.end(); ++it){
		itAfter = it;
		++itAfter;
		for(; itAfter != bamFileNames.end(); ++itAfter){
			if((*it) == (*itAfter))
				throw "The bam file '" + *it + "' has been given multiple times!";
		}
	}

	//read number of chromosomes per sample
	std::vector<int> sampleSizeVec;
	if(params.parameterExists("sampleSize")){
		params.fillParameterIntoVector("sampleSize", sampleSizeVec, ',');
		if(sampleSizeVec.size() != numBamFiles)
			throw "Number of bam files does not match number of sample sizes provided!";
		sampleSizeKnown = true;
	} else sampleSizeKnown = false;

	//Now open BAM files
	logfile->startIndent("Will open the following " + toString(bamFileNames.size()) + " bam files for reading:");
	bamFiles = new TBamFile[numBamFiles];
	int i = 0;
	for(std::vector<std::string>::iterator it = bamFileNames.begin(); it != bamFileNames.end(); ++it, ++i){
		if(sampleSizeKnown)
			logfile->listFlush("Opening BAM file '" + *it + "' containing " + toString(sampleSizeVec.at(i)) + " samples (chromosomes) ...");
		else
			logfile->listFlush("Opening BAM file '" + (std::string) *it + "' of unknown sample size ...");

		bamFiles[i].openBamFile(*it, windowSize, refAndMask, logfile);

		if(sampleSizeKnown)
			bamFiles[i].setNumChromosomes(sampleSizeVec.at(i));

		logfile->write(" done!");
	}
	logfile->endIndent();

	//outputname
	outputName = params.getParameterStringWithDefault("out", "");
	if(outputName == ""){
		//guess from filename
		outputName = bamFileNames[0];
		outputName = extractBeforeLast(outputName, ".");
	}
	logfile->list("Writing output files with prefix '" + outputName + "'.");

	//read chromosomes from first bam file
	bamFiles[0].fillVectorOfChromosomes(chromosomes);
	chrIt = chromosomes.end();

	//limit chrs and / or windows
	useChromosome = new bool[chromosomes.size()];
	if(params.parameterExists("chr")){
		logfile->startIndent("Will limit analysis to the following chromosomes:");

		//set all chromosomes to false
		for(unsigned int i=0; i<chromosomes.size(); ++i)
				useChromosome[i] = false;

		//parse chromosome names
		std::vector<std::string> vec;
		fillVectorFromString(params.getParameterString("chr"), vec, ',');
		int num;
		for(std::vector<std::string>::iterator it=vec.begin(); it!=vec.end(); ++it){
			//find chromosome
			num = 0;
			for(chrIt = chromosomes.begin(); chrIt != chromosomes.end(); ++chrIt, ++num){
				if(*chrIt == *it){
					useChromosome[num] = true;
					logfile->list(*it);
					break;
				}
			}
			if(chrIt == chromosomes.end()) throw "Chromosome '" + *it + "' is not present in the bam header of the firts bam file!";
		}
		chrIt = chromosomes.end();
		limitChr = 1000000;
		logfile->endIndent();
	} else {
		limitChr = params.getParameterIntWithDefault("limitChr", 1000000);
		if(params.parameterExists("limitChr")) logfile->list("Will limit analysis to the first " + toString(limitChr) + " chromosomes.");
		for(int i=0; i<bamFiles[0].bamHeader.Sequences.Size(); ++i)
			useChromosome[i] = true;
	}
	limitWindows = params.getParameterLongWithDefault("limitWindows", 1000000000);
	if(params.parameterExists("limitWindows")) logfile->list("Will limit analysis to the first " + toString(limitWindows) + " windows per chromosome.");
};

void TGenome::jumpToEnd(){
	for(int i=0; i<numBamFiles; ++i)
		bamFiles[i].jumpToEnd();
	chrNumber = -1;
}

void TGenome::restartChromosome(){
	chrIt = chromosomes.begin();
	moveBamFilesToChromosome(*chrIt);
}

bool TGenome::iterateChromosome(){
	if(chrIt == chromosomes.end()){
		chrIt = chromosomes.begin();
		chrNumber = 0;
	} else {
		logfile->endNumbering();
		//move to next
		++chrIt;
		++chrNumber;
	}

	//do we use this chromosome? if not, move on!
	while(chrIt != chromosomes.end() && !useChromosome[chrNumber]){
		++chrIt;
		++chrNumber;
	}

	//did we reach end?
	if(chrIt == chromosomes.end() || chrNumber >= limitChr){
		chrIt = chromosomes.end();
		return false;
	}

	//do we limit chromosomes?
	if(chrNumber >= limitChr) return false;

	//advance bam files and mask
	moveBamFilesToChromosome(*chrIt);
	refAndMask->setChr(*chrIt);

	return true;
}


void TGenome::moveBamFilesToChromosome(std::string & chr){
	logfile->startNumbering("Parsing chromosome '" + chr + "':");

	bamFiles[0].setChr(chr);
	chrLength = bamFiles[0].chrLength;
	for(int i=1; i<numBamFiles; ++i){
		if(!bamFiles[i].setChr(chr))
			throw "Chromosome '" + chr + "' does not exist in bam file '" + bamFiles[i].filename + "'!";
		if(bamFiles[i].chrLength != chrLength)
			throw "Chromosome '" + chr + "' has different length in bam file '" + bamFiles[i].filename + "'!";
	}

	//set indexes
	windowNumber = 0;
	numWindowsOnChr = ceil(chrLength / (double) windowSize);
}


bool TGenome::iterateWindow(){
	if(windowNumber >= limitWindows) return false;
	if(bamFiles[0].curEnd > 0) logfile->endIndent();

	//move to next region
	long curStart = bamFiles[0].iterateWindow();
	if(curStart < 0) return false; //means we reached end of chromosome
	for(int i=1; i<numBamFiles; ++i){
		if(bamFiles[i].iterateWindow() != curStart)
			throw "Problem advancing windows in bam file '" + bamFiles[i].filename + "'!";
	}

	++windowNumber;

	//report
	logfile->number("Window [" + toString(curStart) + ", " + toString(bamFiles[0].curEnd) + ") of " + toString(numWindowsOnChr) + " on '" + *chrIt + "':");
	logfile->addIndent();

	return true;
};

bool TGenome::readData(){
	logfile->startIndent("Reading data:");
	bool hasData = false;
	for(int i=0; i<numBamFiles; ++i){
		if(bamFiles[i].readData())
			hasData = true;
	}
	logfile->endIndent();
	return hasData;
};

void TGenome::initializeRandomGenerator(TParameters & params){
	logfile->listFlush("Initializing random generator ...");

	if(params.parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("fixedSeed"), true);
	} else if(params.parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->write(" done with seed " + toString(randomGenerator->usedSeed) + "!");
	randomGeneratorInitialized = true;
}

float TGenome::getLog10Pvalue_LRT(float & D){
	if(D<=0.0) return 0.0;
	if(D > 69.07915) return -15.0;

	//chisg log10 pval is linear with distance at DF = 2 and log10 pval = 0 at D = 0
 	return chisq_log10_pvalue_max / chisq_distance_max * D;
}

void TGenome::printPileup(TParameters & params){
	//open output
	std::ofstream out;
	std::string filename = outputName + "_pileup.txt";
	out.open(filename.c_str());
	if(!out) throw "Failed to open output file '" + outputName + "'!";

	//write header
	TGenotypeMap genoMap;
	out << "Chr\tposition";
	for(int p=0; p<numBamFiles; ++p)
		out << "\tdeph_" << bamFiles[p].filenameTag << "\tbases_" << bamFiles[p].filenameTag;
	out << "\n";

	//iterate through windows
	while(iterateChromosome()){
		while(iterateWindow()){
			//read data for current window
			readData();

			//print pileup
			logfile->listFlush("Compiling pileup ...");
			for(int i=0; i<bamFiles[0].windowPair.curPointer->length; ++i){
				out << *chrIt << "\t" << bamFiles[0].getCurPos(i) + 1;
				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].printPileup(out, i);
				out << "\n";
			}
			logfile->write(" done!");
		}
	}

	//clean up
	out.close();
}

//---------------------------------------------------------------
//AFL
//---------------------------------------------------------------
void TGenome::calculateAlleleFrequencyLikelihoods(TParameters & params){
	//check if sample sizes are given
	if(!sampleSizeKnown) throw "Can not calculate AFL: sample sizes have not been given!";

	//some parameters
	int minVariantQ;
	bool restrictToPolymorphic = false;
	if(params.parameterExists("minVariantQ")){
		minVariantQ = params.getParameterDouble("minVariantQ");
		logfile->list("Will only use sites considered variants with quality >= " + toString(minVariantQ));
		restrictToPolymorphic = true;
	} else
		logfile->list("Will use all sites with data, also those not considered variable");

	//open output file
	gz::ogzstream outFile;
	std::string outputFileName = outputName + "_sites.txt.gz";
	logfile->list("Writing Allele Frequency Likelihoods to '" + outputFileName + "'");
	outFile.open(outputFileName.c_str());
	if(!outFile) throw "Failed to open output file '" + outputFileName + "'!";

	//write header
	outFile << "chr\tpos";
	if(refAndMask->fastaReference) outFile << "\tref";
	outFile << "\tAllele1\tAllele2\tQ";
	for(int p=0; p<numBamFiles; ++p){
		outFile << "\tf_" << p+1;
		//bamFiles[p].writeAlleleFrequencyLikelihoodHeader(outFile, "\tL_" + toString(p+1) + "_");
	}
	outFile << "\n";

	//some variables
	float** alleleFrequencyLikelihoodAtMLE = new float*[genoMap.numHeterozygousGenotypes];
	for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c)
		alleleFrequencyLikelihoodAtMLE[c] = new float[windowSize];

	float** alleleFrequencyLikelihoodAtMonomorphic = new float*[4];
	for(int c=0; c<4; ++c)
		alleleFrequencyLikelihoodAtMonomorphic[c] = new float[windowSize];

	Base** alleles = new Base*[windowSize];
	for(int i=0; i<windowSize; ++i)
		alleles[i] = new Base[2];

	int mleCombination; int mleMonomorphic;
	float D; float log10Pval; int quality;
	bool hasData;
	bool useSite;
	int curWindowSize;
	int p, c, i;
	float pooledMLEFreq; float pooledNumChr;

	//open saf files
	logfile->listFlush("Open saf files for writing ...");
	for(p=0; p<numBamFiles; ++p)
		bamFiles[p].openSafFile();
	logfile->write(" done!");
	float val = params.getParameterDoubleWithDefault("truncateAFL", -100);
	logfile->list("Will truncate small log(AFLs) to " + toString(val));
	if(val > -10) throw "Can not truncate at a value > -10!";
	for(p=0; p<numBamFiles; ++p)
		bamFiles[p].setSmallestAFLToPrint(val);

	//now parse through genome and calculate AFLs for each site
	while(iterateChromosome()){
		while(iterateWindow()){
			//read reference
			if(refAndMask->fastaReference)
				refAndMask->fillReference(chrNumber, bamFiles[0].curStart, bamFiles[0].curEnd);

			//read data for current window
			if(readData()){
				logfile->listFlush("Calculating Allele Frequency Likelihoods ...");
				curWindowSize = bamFiles[0].curWindowSize;

				//calculate all Allele frequency likelihoods
				for(p=0; p<numBamFiles; ++p)
					bamFiles[p].calcAlleleFrequencyLikelihoods();

				logfile->write(" done!");

				//get LL @ MLE of allele combinations
				logfile->listFlush("Finding MLEs of allele frequencies ...");
				for(c=0; c<genoMap.numHeterozygousGenotypes; ++c){
					for(i=0; i<curWindowSize; ++i)
						alleleFrequencyLikelihoodAtMLE[c][i] = 0.0;
				}
				for(c=0; c<4; ++c){
					for(i=0; i<curWindowSize; ++i)
						alleleFrequencyLikelihoodAtMonomorphic[c][i] = 0.0;
				}

				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].addToAlleleFrequencyLikelihoodAtMLE(alleleFrequencyLikelihoodAtMLE, alleleFrequencyLikelihoodAtMonomorphic);

				logfile->write(" done!");

				//now decide on alleles
				logfile->listFlush("Writing allele frequency likelihoods ...");
				for(i=0; i<curWindowSize; ++i){
					//check if site has data
					hasData = bamFiles[0].siteHasData(i);
					for(p=1; p<numBamFiles; ++p)
						hasData = hasData & bamFiles[p].siteHasData(i);

					if(hasData){
						//check combinations
						mleCombination = 0;
						for(c=1; c<genoMap.numHeterozygousGenotypes; ++c){
							if(alleleFrequencyLikelihoodAtMLE[c][i] > alleleFrequencyLikelihoodAtMLE[mleCombination][i])
								mleCombination = c;
						}

						//check monomorphic
						mleMonomorphic = 0;
						for(c=1; c<4; ++c){
							if(alleleFrequencyLikelihoodAtMonomorphic[c][i] > alleleFrequencyLikelihoodAtMonomorphic[mleMonomorphic][i])
								mleMonomorphic = c;
						}

						//do we have a variant? -> make likelihood ratio test!
						D = - 2.0 * (alleleFrequencyLikelihoodAtMonomorphic[mleMonomorphic][i] - alleleFrequencyLikelihoodAtMLE[mleCombination][i]);
						log10Pval = getLog10Pvalue_LRT(D);
						quality = (int) abs(-10.0 * log10Pval);

						if(restrictToPolymorphic && quality < minVariantQ)
							useSite = false;
						else
							useSite = true;

						//report
						if(useSite){
							outFile << *chrIt << "\t" << bamFiles[0].curStart + i + 1;

							//add reference
							if(refAndMask->fastaReference) outFile << "\t" << refAndMask->referenceForWindow[i];

							//do we flip so we get MAF?
							pooledMLEFreq = 0.0; pooledNumChr = 0.0;
							for(p=0; p<numBamFiles; ++p)
								bamFiles[p].addToPooledMLEAlleleFrequency(i, mleCombination, pooledMLEFreq, pooledNumChr);
							pooledMLEFreq /= pooledNumChr;

							//add alleles
							if(pooledMLEFreq > 0.5)
								outFile << "\t" << genoMap.getHeterozygousGenotypeStringFlipped(mleCombination, '\t');
							else
								outFile << "\t" << genoMap.getHeterozygousGenotypeString(mleCombination, '\t');

							//add p-value (phred scaled)
							outFile << '\t' << quality;


							//MLE estimate of allele frequency
							if(pooledMLEFreq > 0.5){
								for(p=0; p<numBamFiles; ++p){
									bamFiles[p].writeMLEAlleleFrequencyFlip(outFile, i, mleCombination);
								}
							} else {
								for(p=0; p<numBamFiles; ++p){
									bamFiles[p].writeMLEAlleleFrequency(outFile, i, mleCombination);
								}
							}

							//add AFL for MLE allelic combination
							//for(int p=0; p<numBamFiles; ++p)
								//bamFiles[p].writeAlleleFrequencyLikelihoods(outFile, i, mleCombination);

							//add bases (pileup) -> only debugging!
							/*
							outFile << "\t" << bamFiles[0].windowPair.curPointer->sites[i].getBases();
							for(p=1; p<numBamFiles; ++p)
								outFile << "|" << bamFiles[p].windowPair.curPointer->sites[i].getBases();

							for(p=0; p<numBamFiles; ++p){
								for(c=0; c<genoMap.numHeterozygousGenotypes; ++c)
									bamFiles[p].writeAlleleFrequencyLikelihoods(outFile, i, c);
							}
							*/

							//end of line
							outFile << "\n";

							//add to SAf files
							if(pooledMLEFreq > 0.5){
								for(p=0; p<numBamFiles; ++p)
									bamFiles[p].writeAlleleFrequencyLikelihoodsToSafFileFlipOrder(*chrIt, i, mleCombination);
							} else {
								for(p=0; p<numBamFiles; ++p)
									bamFiles[p].writeAlleleFrequencyLikelihoodsToSafFile(*chrIt, i, mleCombination);
							}
						}
					}
				}
				logfile->write(" done!");

			} else logfile->list("No data in this window.");
		}
	}

	//clean up
	outFile.close();
}

//---------------------------------------------------------------
//ApporxWF input
//---------------------------------------------------------------

void TGenome::parseTimePoints(std::string timePointString, std::vector< std::vector<int> > & timePointVec, int & totTimePoints){
	//format is [1,10,23],[1,23] in case of two replicates with 3 and 2 time points, respectively.

	logfile->startIndent("Parsing time points from string:");
	logfile->listFlush("Parsing ...");

	totTimePoints = 0;
	timePointVec.clear();

	//parse time points enclosed in []
	trimString(timePointString);
	while(!timePointString.empty()){
		if(timePointString[0]!='[') throw "Unable to understand time points: missing [!";
		//find closing ]
		std::size_t pos = timePointString.find_first_of(']');
		if(pos == std::string::npos) throw "Unable to understand sample groups: missing ]!";
		std::string tmp = timePointString.substr(1, pos-1);
		timePointString.erase(0,pos+2);
		trimString(timePointString);

		//parse replicate
		trimString(tmp);
		if(tmp.empty()) throw "Unable to understand time points: replicate is empty!";

		std::string temp;
		std::vector<int> tmpVec;
		while(!tmp.empty()){
			temp = extractBefore(tmp, ',');
			trimString(temp);
			tmp.erase(0,1);
			if(!temp.empty()){
				trimString(temp);
				tmpVec.push_back(stringToInt(temp));
				++totTimePoints;
			}
		}

		timePointVec.push_back(tmpVec);
	}
	logfile->done();

	if(timePointVec.size() < 1)
		throw "No time points read!";

	//print time points to log file
	logfile->startIndent("Read the following " + toString(totTimePoints) + " time points:");

	for(size_t r=0; r<timePointVec.size(); ++r){
		logfile->listFlush("replicate ", r+1, ": ");
		for(size_t s=0; s<timePointVec[r].size(); ++s){
			if(s>0) logfile->flush(", ");
			logfile->flush(timePointVec[r][s]);
		}
		logfile->newLine();
	}

	logfile->endIndent();
	logfile->endIndent();
}

void TGenome::generateApproxWFInput(TParameters & params){
	//check if sample sizes are given
	if(!sampleSizeKnown) throw "Can not prepare approxWF input: sample sizes have not been given!";

	//read time points: same order as bam files
	std::vector< std::vector<int> > timePointVec;
	int totTimePoints;
	int numReplicates;
	parseTimePoints(params.getParameterString("timePoints"), timePointVec, totTimePoints);
	if(totTimePoints != numBamFiles)
		throw "Number of provided time points does not match number of bam files!";
	numReplicates = timePointVec.size();

	//some parameters
	int minVariantQ;
	int minNumPoolsWithMinor;
	bool restrictToPolymorphic = false;
	if(params.parameterExists("minVariantQ")){
		minVariantQ = params.getParameterDouble("minVariantQ");
		minNumPoolsWithMinor = params.getParameterIntWithDefault("numPoolsWithMinor", 1);
		logfile->list("Will only use sites considered variants with quality >= " + toString(minVariantQ) + " and for which " + toString(minNumPoolsWithMinor) + " pools have the minor allele.");
		restrictToPolymorphic = true;
	} else {
		if(params.parameterExists("numPoolsWithMinor"))
			throw "Can not restrict to polymorphic pools without a provided minimal variant quality (minVariantQ)!";

		logfile->list("Will use all sites with data, also those not considered variable");
	}

	//open output files: one per pool
	gz::ogzstream* outFiles = new gz::ogzstream[numReplicates];
	logfile->startIndent("Writing transposed approxWF input files to:");
	for(int r=0; r<numReplicates; ++r){
		std::string outputFileName = outputName + "_approxWF_loci_replicate_" + toString(r+1) + ".txt.gz";
		outFiles[r].open(outputFileName.c_str());
		if(!outFiles[r]) throw "Failed to open output file '" + outputFileName + "'!";

		//write header
		outFiles[r] << "Locus";
		for(size_t t=0; t<timePointVec[r].size(); ++t)
			outFiles[r] << "\t" << timePointVec[r][t];
		outFiles[r] << "\n";
	}

	//some variables
	float** alleleFrequencyLikelihoodAtMLE = new float*[genoMap.numHeterozygousGenotypes];
	for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c)
		alleleFrequencyLikelihoodAtMLE[c] = new float[windowSize];

	float** alleleFrequencyLikelihoodAtMonomorphic = new float*[4];
	for(int c=0; c<4; ++c)
		alleleFrequencyLikelihoodAtMonomorphic[c] = new float[windowSize];

	bool useSite;

	//now parse through genome and calculate AFLs for each site
	while(iterateChromosome()){
		while(iterateWindow()){
			//read data for current window
			if(readData()){
				logfile->listFlush("Calculating Allele Frequency Likelihoods ...");
				int curWindowSize = bamFiles[0].curWindowSize;

				//calculate all Allele frequency likelihoods
				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].calcAlleleFrequencyLikelihoods();

				logfile->write(" done!");

				//get LL @ MLE of allele combinations
				logfile->listFlush("Finding MLEs of allele frequencies ...");
				for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
					for(int i=0; i<curWindowSize; ++i)
						alleleFrequencyLikelihoodAtMLE[c][i] = 0.0;
				}
				for(int c=0; c<4; ++c){
					for(int i=0; i<curWindowSize; ++i)
						alleleFrequencyLikelihoodAtMonomorphic[c][i] = 0.0;
				}

				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].addToAlleleFrequencyLikelihoodAtMLE(alleleFrequencyLikelihoodAtMLE, alleleFrequencyLikelihoodAtMonomorphic);

				logfile->write(" done!");

				//now decide on alleles
				logfile->listFlush("Writing allele frequencies to ApproxWF input files ...");
				for(int i=0; i<curWindowSize; ++i){
					//check if site has data
					bool hasData = bamFiles[0].siteHasData(i);
					for(int p=1; p<numBamFiles; ++p)
						hasData = hasData & bamFiles[p].siteHasData(i);

					if(hasData){
						//identify MLE allele combination
						int mleCombination = 0;
						for(int c=1; c<genoMap.numHeterozygousGenotypes; ++c){
							if(alleleFrequencyLikelihoodAtMLE[c][i] > alleleFrequencyLikelihoodAtMLE[mleCombination][i])
								mleCombination = c;
						}

						//check monomorphic
						int mleMonomorphic = 0;
						for(int c=1; c<4; ++c){
							if(alleleFrequencyLikelihoodAtMonomorphic[c][i] > alleleFrequencyLikelihoodAtMonomorphic[mleMonomorphic][i])
								mleMonomorphic = c;
						}

						//do we have a variant? -> make likelihood ratio test
						float D = - 2.0 * (alleleFrequencyLikelihoodAtMonomorphic[mleMonomorphic][i] - alleleFrequencyLikelihoodAtMLE[mleCombination][i]);
						float log10Pval = getLog10Pvalue_LRT(D);
						float quality = -10.0 * log10Pval;

						if(restrictToPolymorphic && quality < minVariantQ)
							useSite = false;
						else
							useSite = true;

						if(useSite && minNumPoolsWithMinor > 1){
							//count number of pools with minor allele
							//do we flip so we get MAF?
							float pooledMLEFreq = 0.0; float pooledNumChr = 0.0;
							for(int p=0; p<numBamFiles; ++p)
								bamFiles[p].addToPooledMLEAlleleFrequency(i, mleCombination, pooledMLEFreq, pooledNumChr);
							pooledMLEFreq /= pooledNumChr;

							//count pools with minor allele
							int numPoolsWithMinor = 0;
							if(pooledMLEFreq <= 0.5){
								for(int p=1; p<numBamFiles; ++p){
									if(bamFiles[p].getMLEAlleleFrequency(i, mleCombination) > 0.0)
										++numPoolsWithMinor;
								}
							} else {
								for(int p=1; p<numBamFiles; ++p){
									if(bamFiles[p].getMLEAlleleFrequencyFlip(i, mleCombination) > 0.0)
										++numPoolsWithMinor;
								}
							}

							std::cout << "@@ " << *chrIt << "_" << bamFiles[0].curStart + i + 1 << " " << numPoolsWithMinor;
							for(int p=1; p<numBamFiles; ++p)
								std::cout << " " << bamFiles[p].getMLEAlleleFrequency(i, mleCombination) << "|" << bamFiles[p].getMLEAlleleCounts(i, mleCombination);
							std::cout << std::endl;


							if(numPoolsWithMinor < minNumPoolsWithMinor)
								useSite = false;
						}

						//write site to ApproxWF input files, if used.
						if(useSite){
							int bamIndex = 0;
							for(int r=0; r<numReplicates; ++r){
								outFiles[r] << *chrIt << "_" << bamFiles[0].curStart + i + 1;
								for(int t=0; t<timePointVec[r].size(); ++t){
									bamFiles[bamIndex].writeMLEAlleleCountToApproxWFFile(outFiles[r], i, mleCombination);
									++bamIndex;
								}
								outFiles[r] << "\n";
							}
						}
					}
				}
				logfile->write(" done!");

			} else logfile->list("No data in this window.");
		}
	}

	//clean up
	for(int r=0; r<numReplicates; ++r)
		outFiles[r].close();
}

//---------------------------------------------------------------
//find strictly monomorphic sites
//---------------------------------------------------------------
void TGenome::findStrictlyMonomoprhicSites(TParameters & params){
	//read parameters
	int minDepth = params.getParameterIntWithDefault("minDepth", 10);
	int minPopWithSufficientDepth = params.getParameterIntWithDefault("minPop", 1);
	logfile->list("Will only consider sites for which at least " + toString(minPopWithSufficientDepth) + " have a depth of at least " + toString(minDepth) + ".");


	//open output file
	gz::ogzstream outFile;
	std::string outputFileName = outputName + "_monomorphicSites.txt.gz";
	logfile->list("Writing monomorphic sites to '" + outputFileName + "'");
	outFile.open(outputFileName.c_str());
	if(!outFile) throw "Failed to open output file '" + outputFileName + "'!";

	//write header
	outFile << "chr\tpos";
	if(refAndMask->fastaReference) outFile << "\tref";
	outFile << "\tAllele1\tAllele2\tQ";
	for(int p=0; p<numBamFiles; ++p){
		outFile << "\tf_" << p+1;
		//bamFiles[p].writeAlleleFrequencyLikelihoodHeader(outFile, "\tL_" + toString(p+1) + "_");
	}
	outFile << "\n";

	//some variables
	int*** baseCounts = new int**[numBamFiles];
	int** depth = new int*[numBamFiles];
	for(int p=0; p<numBamFiles; ++p){
		baseCounts[p] = new int*[windowSize];
		depth[p] = new int[windowSize];
		for(int i=0; i<windowSize; ++i)
			baseCounts[p][i] = new int[5];
	}
	int** totalBaseCounts = new int*[windowSize];
	for(int i=0; i<windowSize; ++i)
		totalBaseCounts[i] = new int[4];

	int curWindowSize;

	//open saf files
	logfile->listFlush("Open saf files for writing ...");
	for(int p=0; p<numBamFiles; ++p)
		bamFiles[p].openSafFile();
	logfile->write(" done!");

	//now parse through genome and calculate AFLs for each site
	while(iterateChromosome()){
		while(iterateWindow()){
			//read reference
			if(refAndMask->fastaReference)
				refAndMask->fillReference(chrNumber, bamFiles[0].curStart, bamFiles[0].curEnd);

			//read data for current window
			if(readData()){
				curWindowSize = bamFiles[0].curWindowSize;

				//fill base counts
				logfile->listFlush("Assembling base counts ...");
				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].fillBaseCounts(baseCounts[p]);

				//calc depth per site
				for(int i=0; i<curWindowSize; ++i){
					totalBaseCounts[i][0] = 0;
					totalBaseCounts[i][1] = 0;
					totalBaseCounts[i][2] = 0;
					totalBaseCounts[i][3] = 0;
				}
				for(int p=0; p<numBamFiles; ++p){
					for(int i=0; i<curWindowSize; ++i){
						depth[p][i] = baseCounts[p][i][0] + baseCounts[p][i][1] + baseCounts[p][i][2] + baseCounts[p][i][3];

						totalBaseCounts[i][0] += baseCounts[p][i][0];
						totalBaseCounts[i][1] += baseCounts[p][i][1];
						totalBaseCounts[i][2] += baseCounts[p][i][2];
						totalBaseCounts[i][3] += baseCounts[p][i][3];
					}
				}
				logfile->write(" done!");


				//Now check for each site
				logfile->listFlush("Identifying monomorphic sites ...");
				//loop over sites
				for(int i=0; i<curWindowSize; ++i){
					//check if we have enough depth in enough populations
					int numPopEnoughDepth = 0;
					for(int p=0; p<numBamFiles; ++p){
						if(depth[p][i] >= minDepth)
							++numPopEnoughDepth;
					}

					if(numPopEnoughDepth > minPopWithSufficientDepth){
						//check if site is monomorphic
						int numBasesWithZeroCounts = 0;
						for(int b=0; b<4; ++b){
							if(totalBaseCounts[i][b] == 0)
								++numBasesWithZeroCounts;
						}

						if(numBasesWithZeroCounts == 3){
							//write site
							outFile << *chrIt << "\t" << bamFiles[0].curStart + i << "\t" << bamFiles[0].curStart + i + 1 << "\n";
						}
					}
				}
				logfile->write(" done!");

			} else logfile->list("No data in this window.");
		}
	}

	//clean up
	outFile.close();

	for(int p=0; p<numBamFiles; ++p){
		for(int i=0; i<windowSize; ++i)
			delete[] baseCounts[p][i];
		delete[] baseCounts[p];
		delete[] depth[p];
	}
	delete[] baseCounts;
	delete[] depth;
	for(int i=0; i<windowSize; ++i)
		delete[] totalBaseCounts[i];
	delete[] totalBaseCounts;
}


//---------------------------------------------------------------
//pool stat (unknown sample size)
//---------------------------------------------------------------
void TGenome::poolStat(TParameters & params){
	//check if sample sizes are given
	if(sampleSizeKnown) throw "Use task AFL if sample size is known!";

	//some parameters
	int maxNumIterations = params.getParameterIntWithDefault("maxNR", 100);
	float maxDeltaF = params.getParameterDoubleWithDefault("maxDeltaF", 0.0001);

	int minVariantQ;
	bool restrictToPolymorphic = false;
	if(params.parameterExists("minVariantQ")){
		minVariantQ = params.getParameterDouble("minVariantQ");
		logfile->list("Will only use sites considered variants with quality >= " + toString(minVariantQ));
		restrictToPolymorphic = true;
	} else
		logfile->list("Will use all sites with data, also those not considered variable");


	//open output file
	gz::ogzstream outFile;
	std::string outputFileName = outputName + "_poolStat.txt.gz";
	logfile->list("Writing Allele Frequency estimates and statistics to '" + outputFileName + "'");
	outFile.open(outputFileName.c_str());
	if(!outFile) throw "Failed to open output file '" + outputFileName + "'!";

	//write header
	outFile << "chr\tpos";
	if(refAndMask->fastaReference) outFile << "\tref";
	outFile << "\tAllele1\tAllele2\tQ";
	for(int p=0; p<numBamFiles; ++p){
		outFile << "\tf_" << p+1;
	}
	for(int p=0; p<numBamFiles; ++p){
		outFile << "\tH_" << p+1;
	}
	for(int p=0; p<numBamFiles-1; ++p){
		for(int q=p+1; q<numBamFiles; ++q){
			outFile << "\tFst_" << p+1 << "_" << q+1;
		}
	}
	outFile << "\n";

	//some variables
	float** alleleFrequencyLikelihoodAtMLE = new float*[genoMap.numHeterozygousGenotypes];
	for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c)
		alleleFrequencyLikelihoodAtMLE[c] = new float[windowSize];

	float** alleleFrequencyLikelihoodAtMonomorphic = new float*[4];
	for(int c=0; c<4; ++c)
		alleleFrequencyLikelihoodAtMonomorphic[c] = new float[windowSize];

	Base** alleles = new Base*[windowSize];
	for(int i=0; i<windowSize; ++i){
		alleles[i] = new Base[2];
	}
	float* MLE_alleleFrequencies = new float[numBamFiles];
	float* heterozygosities = new float[numBamFiles];

	int mleCombination; int mleMonomorphic;
	float D; float log10Pval;
	bool useSite;
	int curWindowSize;
	float f_T, H_T, H_S, Fst;
	float quality;
	int p;
	bool hasData;

	//now parse through genome and estimate allele frequencies for each site
	while(iterateChromosome()){
		while(iterateWindow()){
			//read reference
			if(refAndMask->fastaReference)
				refAndMask->fillReference(chrNumber, bamFiles[0].curStart, bamFiles[0].curEnd);

			//read data for current window
			if(readData()){
				logfile->listFlush("Estimating MLE frequecies ...");
				curWindowSize = bamFiles[0].curWindowSize;

				//calculate all Allele frequency likelihoods
				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].findMLEFrequenciesPooled(maxNumIterations, maxDeltaF);

				//get LL @ MLE of allele combinations
				for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
					for(int i=0; i<curWindowSize; ++i)
						alleleFrequencyLikelihoodAtMLE[c][i] = 0.0;
				}
				for(int c=0; c<4; ++c){
					for(int i=0; i<curWindowSize; ++i)
						alleleFrequencyLikelihoodAtMonomorphic[c][i] = 0.0;
				}

				for(int p=0; p<numBamFiles; ++p)
					bamFiles[p].addToPooledFrequencyLikelihoodAtMLE(alleleFrequencyLikelihoodAtMLE, alleleFrequencyLikelihoodAtMonomorphic);

				logfile->write(" done!");

				//now decide on alleles
				logfile->listFlush("Finding MLE of alleleic combinations ...");
				for(int i=0; i<curWindowSize; ++i){
					//check if site has data
					hasData = bamFiles[0].siteHasData(i);
					for(p=1; p<numBamFiles; ++p)
						hasData = hasData & bamFiles[p].siteHasData(i);

					if(hasData){
						//check combinations
						mleCombination = 0;
						for(int c=1; c<genoMap.numHeterozygousGenotypes; ++c){
							if(alleleFrequencyLikelihoodAtMLE[c][i] > alleleFrequencyLikelihoodAtMLE[mleCombination][i])
								mleCombination = c;
						}

						//check monomorphic
						mleMonomorphic = 0;
						for(int c=1; c<4; ++c){
							if(alleleFrequencyLikelihoodAtMonomorphic[c][i] > alleleFrequencyLikelihoodAtMonomorphic[mleMonomorphic][i])
								mleMonomorphic = c;
						}

						//do we have a variant? -> make likelihood ratio test!
						D = - 2.0 * (alleleFrequencyLikelihoodAtMonomorphic[mleMonomorphic][i] - alleleFrequencyLikelihoodAtMLE[mleCombination][i]);
						log10Pval = getLog10Pvalue_LRT(D);
						quality = (int) abs(-10.0 * log10Pval);

						if(restrictToPolymorphic && quality < minVariantQ)
							useSite = false;
						else
							useSite = true;

						//report
						if(useSite){
							outFile << *chrIt << "\t" << bamFiles[0].curStart + i + 1;
							//add reference
							if(refAndMask->fastaReference) outFile << "\t" << refAndMask->referenceForWindow[i];
							//add alleles
							outFile << "\t" << genoMap.getHeterozygousGenotypeString(mleCombination, '\t');
							//add bases (pileup) -> only debugging!
							//outFile << "\t" << bamFiles[0].windowPair.curPointer->sites[i].getBases();
							//outFile << "|" << bamFiles[1].windowPair.curPointer->sites[i].getBases();
							//add p-value (phred scaled)
							outFile << '\t' << abs(-10.0 * log10Pval);
							//MLE estimate of allele frequency
							for(int p=0; p<numBamFiles; ++p){
								MLE_alleleFrequencies[p] = bamFiles[p].getMLEAlleleFrequencyPooled(i, mleCombination);
								if(MLE_alleleFrequencies[p] < 0.0)
									outFile << "\t-";
								else {
									outFile << "\t" << MLE_alleleFrequencies[p];
								}
							}

							//calc heterozygosities
							for(p=0; p<numBamFiles; ++p){
								if(MLE_alleleFrequencies[p] < 0.0)
									outFile << "\t-";
								else {
									heterozygosities[p] = 2.0 * MLE_alleleFrequencies[p] * (1.0 - MLE_alleleFrequencies[p]);
									outFile << "\t" << heterozygosities[p];
								}
							}

							//calc Fst
							for(p=0; p<numBamFiles-1; ++p){
								for(int q=p+1; q<numBamFiles; ++q){
									//check if both have data
									if(MLE_alleleFrequencies[p] < 0.0 || MLE_alleleFrequencies[q] < 0.0)
										outFile << "\t-";
									else {
										f_T = (MLE_alleleFrequencies[p] + MLE_alleleFrequencies[q]) / 2.0;
										H_T = 2.0 * f_T * (1.0 - f_T);
										if(H_T <= 0.0)
											Fst = 0.0;
										else {
											H_S = (heterozygosities[p] + heterozygosities[q]) / 2.0;
											Fst = (H_T - H_S)/H_T;
										}
										outFile << "\t" << Fst;
									}
								}
							}
							//end of line
							outFile << "\n";
						}
					}
				}
				logfile->write(" done!");

			} else logfile->list("No data in this window.");
		}
	}
	//clean up
	outFile.close();
}






