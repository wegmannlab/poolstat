/*
 * TWindow.h
 *
 *  Created on: May 17, 2015
 *      Author: wegmannd
 */

#ifndef TWINDOW_H_
#define TWINDOW_H_

#include "TLog.h"
#include "TParameters.h"
#include "TReadGroups.h"
#include "bamtools/utils/bamtools_fasta.h"
#include "TSite.h"
#include "TBedReader.h"
#include "TSiteSubset.h"
#include "omp.h"

//---------------------------------------------------------------
//TWindow
//---------------------------------------------------------------
class TWindow{
public:
	long start;
	long end; //end NOT included in window!
	long length;
	TSite* sites;
	bool sitesInitialized;
	int numReadsInWindow;
	double coverage, coverageAtSitesWithData, fractionSitesNoData, fractionsitesCoverageAtLeastTwo;
	TBaseFrequencies baseFreq;
	TGenotypeMap genoMap;
	float*** alleleFrequencyLikelihoods; //[allelic combination][site][frequency] = LL
	int** MLE_AlleleFrequencyIndex; //[allelic combination][site] = frequency index in alleleFrequencyLikelihoods
	float** MLE_AlleleFrequencies; //[allelic combination][site] = frequency (as double)
	float** MLE_AlleleFrequencies_LL; //[allelic combination][site] = frequency (as double)
	float** MLE_AlleleFrequencies_LL_nullModel; //[allelic combination][site] = frequency (as double)
	int numChromosomes;
	bool alleleFrequencyLikelihoodsInitialized;
	bool alleleFrequencyLikelihoodsCalculated;
	bool alleleFrequencyEstimatesInitialized;

	TWindow();
	TWindow(long Start, long End);
	virtual ~TWindow(){
		if(sitesInitialized) delete[] sites;
		deleteAlleleFrequencyLikelihoods();
	};
	void initSites(long newLength);
	void clear();
	void move(long Start, long End);
	bool addFromRead(BamTools::BamAlignment & bamAlignement, int & minQuality, int & maxQuality, int & maxNumBasesPerSite);
	void addReferenceBaseToSites(BamTools::Fasta & reference, int & refId);
	void addReferenceBaseToSites(TSiteSubset* subset);
	void applyMask(TBedReader* mask);
	void maskCpG(BamTools::Fasta & reference, int & refId);
	void estimateBaseFrequencies();
	void calculateEmissionProbabilities();
	void printPileup(std::ofstream & out, int & pos);
	void calcCoverage();
	void calcCoveragePerSite(long * siteCoverage, unsigned int maxCov);
	void applyCoverageFilter(int minCoverage, int maxCoverage);
	void fillBaseCounts(int** counts);
	bool siteHasData(int & site){return sites[site].hasData;};

	//AFL
	void deleteAlleleFrequencyLikelihoods();
	void initializeAlleleFrequencyLikelihoods(int & NumChromosomes);
	void calculateAlleleFreqLikelihoods(int & NumChromosomes);
	void findMLAlleleFrequencies();
	double getAlleleFrequencyLikelihoodAtMLE(const int & site, const int & alleleicCombination);
	double getMLEAlleleFrequency(const int & site, const int & alleleicCombination);
	int getMLEAlleleCounts(const int & site, const int & alleleicCombination);
	double getAlleleFrequencyLikleihood_monomorphic(int & site, int & allele);
	void addToAlleleFrequencyLikelihoodAtMLE(float** alleleFrequencyLikelihoodAtMLE, float** alleleFrequencyLikelihoodAtMonomorphic);
	void writeAlleleFrequencyLikelihoods(gz::ogzstream & outFile, int & alleleicCombination, int & site);
	float* pointerToAlleleFrequencyLikleihoods(int & alleleicCombination, int & site){
		return alleleFrequencyLikelihoods[alleleicCombination][site];
	};

	//pooled (unknown sample size)
	void initializeAlleleFrequencyEstimates();
	void findMLEFrequenciesPooled(int & maxNumIterations, float & maxDelta);
	void addToPooledFrequencyLikelihoodAtMLE(float** likelihoodAtMLE, float** likelihoodAtMonomorphic);
	double getMLEAlleleFrequencyPooled(int & site, int & alleleicCombination);
};


//---------------------------------------------------------------
//TWindowPair
//---------------------------------------------------------------
class TWindowPair{
public:
	TWindow* curPointer;
	TWindow* nextPointer;

	TWindowPair(){
		curPointer = new TWindow();
		nextPointer = new TWindow();
	};
	~TWindowPair(){
		delete curPointer;
		delete nextPointer;
	};
	void swap(){
		TWindow* tmp = curPointer;
		curPointer = nextPointer;
		nextPointer = tmp;
	};
	void addToCur(BamTools::BamAlignment & bamAlignement, int & minQuality, int & maxQuality, int & maxNumBasesPerSite){
		curPointer->addFromRead(bamAlignement, minQuality, maxQuality, maxNumBasesPerSite);
	};
	void addToNext(BamTools::BamAlignment & bamAlignement, int & minQuality, int & maxQuality, int & maxNumBasesPerSite){
		nextPointer->addFromRead(bamAlignement, minQuality, maxQuality, maxNumBasesPerSite);
	};
	void clear(){
		curPointer->clear();
		nextPointer->clear();
	}
};

#endif /* TWINDOW_H_ */
