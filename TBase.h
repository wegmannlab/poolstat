/*
 * TBase.h
 *
 *  Created on: Oct 17, 2015
 *      Author: wegmannd
 */

#ifndef TBASE_H_
#define TBASE_H_

#include "TGenotypeMap.h"
#include <math.h>

//---------------------------------------------------------------
//TBase
//---------------------------------------------------------------
class TBase{
public:
	int quality;
	double errorRate;
	double errorRateThird;
	double emissionProbabilities[4];

	TBase(char & Quality){
		quality = (int) Quality - 33;
		errorRate = pow(10.0, (double) quality / -10.0);
		errorRateThird = errorRate / 3.0;
	};

	virtual ~TBase(){};

	virtual void fillEmissionProbabilities(){
		throw "Function 'fillEmissionProbabilities' Not implemented for base class TBase!";
	};
	virtual char getBase(){ return '?'; };
	virtual Base getBaseAsEnum(){ return N;};

	virtual void addToBaseFrequencies(TBaseFrequencies & frequencies, double & weight){};

	double getEmissionProbability(Base genotype){
		return emissionProbabilities[genotype];
	};
	double getEmissionProbability(int genotype){
		return emissionProbabilities[genotype];
	};
	void addToExpectedBaseCounts(TBaseFrequencies & baseFreq, double* expectedCounts);

};
//---------------------------------------------------------------
class TBaseHaploidA:public TBase{
public:
	TBaseHaploidA(char & Quality):TBase(Quality){fillEmissionProbabilities();};
	char getBase(){ return 'A'; };
	Base getBaseAsEnum(){ return A;};
	void addToBaseFrequencies(TBaseFrequencies & frequencies, double & weight){ frequencies.add(A, weight); };
	void fillEmissionProbabilities();
};
//---------------------------------------------------------------
class TBaseHaploidC:public TBase{
public:
	TBaseHaploidC(char & Quality):TBase(Quality){fillEmissionProbabilities();};
	char getBase(){ return 'C'; };
	Base getBaseAsEnum(){ return C;};
	void addToBaseFrequencies(TBaseFrequencies & frequencies, double & weight){ frequencies.add(C, weight); };
	void fillEmissionProbabilities();
};
//---------------------------------------------------------------
class TBaseHaploidG:public TBase{
public:
	TBaseHaploidG(char & Quality):TBase(Quality){fillEmissionProbabilities();};
	char getBase(){ return 'G'; };
	Base getBaseAsEnum(){ return G;};
	void addToBaseFrequencies(TBaseFrequencies & frequencies, double & weight){ frequencies.add(G, weight); };
	void fillEmissionProbabilities();
};
//---------------------------------------------------------------
class TBaseHaploidT:public TBase{
public:
	TBaseHaploidT(char & Quality):TBase(Quality){fillEmissionProbabilities();};
	char getBase(){ return 'T'; };
	Base getBaseAsEnum(){ return T;};
	void addToBaseFrequencies(TBaseFrequencies & frequencies, double & weight){ frequencies.add(T, weight); };
	void fillEmissionProbabilities();
};


#endif /* TBASE_H_ */
