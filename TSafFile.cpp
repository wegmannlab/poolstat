/*
 * safFileFunctions.cpp
 *
 *  Created on: Nov 16, 2016
 *      Author: wegmannd
 */

#include "TSafFile.h"

TSafFile::TSafFile(std::string & outputName, int NumChromosomes){
	numChromosomes = NumChromosomes;
	dimension = NumChromosomes + 1;
	numSitesWrittenOnChr = 0;
	smallestAFLPrinted = -99999;
	truncateSmallAFLValues = false;

	//open files
	open(outputName);
}

TSafFile::~TSafFile(){
	close();
}

void TSafFile::open(std::string & outputName){
	//open files
	outfileSAF.open(outputName + ".saf.gz");
	outfileSAFPOS.open(outputName + ".saf.pos.gz");
	outfileSAFIDX = openFile(outputName + ".saf.idx");

	filesOpend = true;
	numSitesWrittenOnChr = 0;

	//write magic string
	char buf[8] = "safv3";
	outfileSAF.write(buf, 8);
	outfileSAFPOS.write(buf,8);
	fwrite(buf,1,8,outfileSAFIDX);

	//write dimensionality to index file
	size_t tt = numChromosomes;
	fwrite(&tt,sizeof(tt),1,outfileSAFIDX);

	//store offsets
	storeCurrentOffset();
	chrName = "";
}

void TSafFile::close(){
	if(filesOpend){
		updateIndex();
		//bgzf_close(outfileSAF);
		//bgzf_close(outfileSAFPOS);
		outfileSAF.close();
		outfileSAFPOS.close();
		fclose(outfileSAFIDX);
		filesOpend = false;
	}
}

FILE* TSafFile::openFile(std::string filename){
	FILE *fp = NULL;
	fp = fopen(filename.c_str(),"w");
	if(fp==NULL)
		throw "Problem opening file '" + filename + "'! Check permissions?";
	return fp;
}

/*
BGZF* TSafFile::openFileBG(std::string filename){
	BGZF *fp = bgzf_open(filename.c_str(),GZOPT);
	if(fp==NULL)
		throw "Problem opening file '" + filename + "'! Check permissions?";
	return fp;
}
*/
/*
ssize_t TSafFile::bgzf_write(BGZF *fp, const void *data, size_t length){
	if(length>0)
		return ::bgzf_write(fp,data,length);
	return 0;
}
*/
/*
ssize_t TSafFile::write_to_bgzf(BGZF *fp, const void *data, size_t length){
	if(length % sizeof(float) != 0)
		throw "length is not a multiple of the size of floats!";

	if(length>0)
        return bgzf_write(fp,data,length);
    return 0;
}
*/

void TSafFile::storeCurrentOffset(){
	//offsetSAF = bgzf_tell(outfileSAF);
	//offsetSAFPOS = bgzf_tell(outfileSAFPOS);
	offsetSAF = outfileSAF.getCurrentOffset();
	offsetSAFPOS = outfileSAFPOS.getCurrentOffset();

	//std::cout << "offsetSAF = " << offsetSAF << " = " <<  bgzf_tell(outfileSAF.filePointer) << std::endl;
}

void TSafFile::updateIndex(){
	if(chrName != "" && numSitesWrittenOnChr > 0){
		size_t clen = chrName.size();
		fwrite(&clen,sizeof(size_t),1,outfileSAFIDX);
		fwrite(chrName.c_str(),1,clen,outfileSAFIDX);
		size_t tt = numSitesWrittenOnChr;
		fwrite(&tt,sizeof(size_t),1,outfileSAFIDX);
		fwrite(&offsetSAFPOS,sizeof(int64_t),1,outfileSAFIDX);
		fwrite(&offsetSAF,sizeof(int64_t),1,outfileSAFIDX);

		//std::cout << "offsets = " << offsetSAF << ", " << offsetSAFPOS << std::endl;

		//store new offsets
		storeCurrentOffset();
		numSitesWrittenOnChr = 0;
	}
}

void TSafFile::setSmallestAFLToPrint(float value){
	smallestAFLPrinted = value;
	truncateSmallAFLValues = true;
}

void TSafFile::writeLogAFLs(std::string & chr, int pos, float* logAFLs){
	//check if are still on the same chromosome
	if(chr != chrName){

		//std::cout << std::endl << "--------------------------------- NEW CHR!!!!!" << std::endl;

		updateIndex();
		chrName = chr;
	}

	//standardize
	float maxVal = logAFLs[0];
	for(int i=1; i<dimension; ++i){
		if(logAFLs[i] > maxVal)
			maxVal = logAFLs[i];
	}

	for(int i=0; i<dimension; ++i)
		logAFLs[i] = logAFLs[i] - maxVal;

	//replace too small values with smallest value accepted
	if(truncateSmallAFLValues){
		for(int i=0; i<dimension; ++i){
			if(logAFLs[i] < smallestAFLPrinted)
				logAFLs[i] = smallestAFLPrinted;
		}
	}

	//write to SAF and SAFPOS
	//if(write_to_bgzf(outfileSAF,logAFLs,sizeof(float)*dimension)){
	//	write_to_bgzf(outfileSAFPOS,&pos,sizeof(int));
	//		++numSitesWrittenOnChr;
	//}

	//std::cout << "Writing AFL: " << std::flush;

	if(outfileSAF.write(logAFLs,sizeof(float)*dimension)){
		outfileSAFPOS.write(&pos,sizeof(int));
		++numSitesWrittenOnChr;
	}
}


//--------------------------------
//Class to read a SAf file
//--------------------------------

TSafFileReader::TSafFileReader(std::string Filename){
	open(Filename);
}

TSafFileReader::~TSafFileReader(){
	close();
}

void TSafFileReader::open(std::string & Filename){
	filename = Filename + ".saf.gz";
	filenamePos = Filename + ".saf.pos.gz";
	filenameIdx = Filename + ".saf.idx";
	posOnChr = 0;
	posRead = 0;

	//open saf file
	inputSafFile = bgzf_open(filename.c_str(),"rb");
	if(inputSafFile == NULL)
		throw "Problem opening SAF file '" + filename + "' for reading! Check permissions?";
	fileIsOpen = true;

	//open pos file
	inputSafPosFile = bgzf_open(filenamePos.c_str(),"rb");
	if(inputSafFile == NULL)
		throw "Problem opening POS file '" + filename + "' for reading! Check permissions?";

	//open idx file
	inputSafIdxFile = fopen(filenameIdx.c_str(), "rb");
	if(inputSafIdxFile == NULL)
		throw "Problem opening IDX file '" + filename + "' for reading! Check permissions?";

	//read first eight bytes which is version number
	char buf[8];
	if(8 != bgzf_read(inputSafFile, buf, 8))
	    throw "Problem reading version tag from SAF file!";
	safVersion = buf;
	if(8 != bgzf_read(inputSafPosFile, buf, 8))
	    throw "Problem reading version tag from POS file!";
	if(8 != fread(buf, 1, 8, inputSafIdxFile))
	    throw "Problem reading version tag from POS file!";

	//read dimensionality and initialize storage
	size_t tt;
	fread(&tt, sizeof(size_t), 1, inputSafIdxFile);
	numChromosomes = tt;
	dimension = numChromosomes + 1;
	sizeOfChunkToRead = dimension*sizeof(float);
	likelihoods = new float[dimension];
	std::string chr;
}

bool TSafFileReader::readChromosomeInfo(){
	//read chromosome name
	size_t tt;
	if(!fread(&tt, sizeof(size_t), 1, inputSafIdxFile))
		return false;

	char* tmp = new char[tt];
	fread(tmp, 1, tt, inputSafIdxFile);
	chromosome = "";
	for(int i=0; i<tt; ++i)
		chromosome += tmp[i];
	delete[] tmp;

	//read size of chromosome
	fread(&posOnChr, sizeof(int64_t), 1, inputSafIdxFile);
	posRead = 0;

	//read offsets
	fread(&offsetSAF, sizeof(int64_t), 1, inputSafIdxFile);
	fread(&offsetSAFPOS, sizeof(int64_t), 1, inputSafIdxFile);

	//std::cout << "OFFSETS = " << offsetSAF << ", " << offsetSAFPOS << std::endl;

	return true;
};


void TSafFileReader::close(){
	if(fileIsOpen){
		bgzf_close(inputSafFile);
	}
	fileIsOpen = false;
	filename = "";
	numChromosomes = -1;
}

bool TSafFileReader::readLine(){
	if(!fileIsOpen)
		throw "Can not read from SAF file: file is not open!";

	//check if we move to next chromosome
	if(posRead == posOnChr){
		if(!readChromosomeInfo())
			return false;
	}

	//now read next line, which means as many values as chromosomes+1
	if(sizeOfChunkToRead != bgzf_read(inputSafFile, likelihoods, sizeOfChunkToRead))
	    return false;

	//also read position
	if(!bgzf_read(inputSafPosFile, &position, sizeof(int)))
	    throw "Failed to read from POS file!";

	++posRead;
	return true;
}


void TSafFileReader::printLine(){
	std::cerr << chromosome << "\t" << position;
	for(int i=0; i<dimension; ++i)
		std::cerr << "\t" << likelihoods[i];
	std::cerr << "\n";
}

































