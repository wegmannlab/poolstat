/*
 * TBase.cpp
 *
 *  Created on: Oct 17, 2015
 *      Author: wegmannd
 */


#include "TBase.h"

//-------------------------------------------------------
//TBase
//-------------------------------------------------------
/*
void TBase::fillEmissionProbabilities(TPMD & pmdObject){
	fillEmissionProbabilitiesCore(pmdObject, errorRate);
}
*/

void TBase::addToExpectedBaseCounts(TBaseFrequencies & baseFreq, double* expectedCounts){
	double sum=0.0;
	for(int b=0; b<4; ++b) sum += emissionProbabilities[b] * baseFreq[b];
	for(int b=0; b<4; ++b) expectedCounts[b] += emissionProbabilities[b] / sum;
}

void TBaseHaploidA::fillEmissionProbabilities(){
	emissionProbabilities[A] = 1.0 - errorRate;
	emissionProbabilities[C] = errorRateThird;
	emissionProbabilities[G] = errorRateThird;
	emissionProbabilities[T] = errorRateThird;
}

void TBaseHaploidC::fillEmissionProbabilities(){
	//pre-calculate all emission probabilities given the error rate and the distance from either end of the read
	emissionProbabilities[A] = errorRateThird;
	emissionProbabilities[C] = 1.0 - errorRate;
	emissionProbabilities[G] = errorRateThird;
	emissionProbabilities[T] = errorRateThird;
}

void TBaseHaploidG::fillEmissionProbabilities(){
	//pre-calculate all emission probabilities given the error rate and the distance from either end of the read
	emissionProbabilities[A] = errorRateThird;
	emissionProbabilities[C] = errorRateThird;
	emissionProbabilities[G] = 1.0 - errorRate;
	emissionProbabilities[T] = errorRateThird;
}

void TBaseHaploidT::fillEmissionProbabilities(){
	//pre-calculate all emission probabilities given the error rate and the distance from either end of the read
	emissionProbabilities[A] = errorRateThird;
	emissionProbabilities[C] = errorRateThird;
	emissionProbabilities[G] = errorRateThird;
	emissionProbabilities[T] = 1.0 - errorRate;
}


