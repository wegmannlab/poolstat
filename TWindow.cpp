/*
 * TWindow.cpp
 *
 *  Created on: May 17, 2015
 *      Author: wegmannd
 */

#include "TWindow.h"

//-------------------------------------------------------
//Twindow
//-------------------------------------------------------
TWindow::TWindow(){
	start = -1;
	end = -1;
	length = -1;
	sites = NULL;
	sitesInitialized = false;
	coverage = -1.0;
	coverageAtSitesWithData = -1.0;
	fractionSitesNoData = -1.0;
	fractionsitesCoverageAtLeastTwo = -1.0;
	numReadsInWindow = 0;
	alleleFrequencyLikelihoods = NULL;
	alleleFrequencyLikelihoods = NULL;
	MLE_AlleleFrequencyIndex = NULL;
	MLE_AlleleFrequencies = NULL;
	MLE_AlleleFrequencies_LL = NULL;
	MLE_AlleleFrequencies_LL_nullModel = NULL;
	numChromosomes = -1;
	alleleFrequencyLikelihoodsInitialized = false;
	alleleFrequencyEstimatesInitialized = false;
	alleleFrequencyLikelihoodsCalculated = false;
};

TWindow::TWindow(long Start, long End){
	start = Start;
	end = End;
	initSites(end - start); //end NOT in window!
	alleleFrequencyLikelihoodsInitialized = false;
	alleleFrequencyEstimatesInitialized = false;
	alleleFrequencyLikelihoodsCalculated = false;
};

void TWindow::initSites(long newLength){
	if(sitesInitialized)
		delete[] sites;
	deleteAlleleFrequencyLikelihoods();
	length = newLength;
	sites = new TSite[length];
	sitesInitialized = true;
	coverage = -1.0;
	fractionSitesNoData = -1.0;
	fractionsitesCoverageAtLeastTwo = -1.0;
	numReadsInWindow = 0;
}

void TWindow::clear(){
	for(int i=0; i<length; ++i) sites[i].clear();
	coverage = -1.0;
	fractionSitesNoData = -1.0;
	fractionsitesCoverageAtLeastTwo = -1.0;
	numReadsInWindow = 0;
};

void TWindow::move(long Start, long End){
	start = Start;
	end = End;
	if(sitesInitialized){
		if((end - start) != length){
			initSites(end - start);
		} else {
			clear();
		}
	} else initSites(end - start);
	alleleFrequencyLikelihoodsCalculated = false;
};

bool TWindow::addFromRead(BamTools::BamAlignment & bamAlignment, int & minQuality, int & maxQuality, int & maxNumBasesPerSite){
	/* Note:
	 * Function returns true if read also maps to next window and
	 * returns false if end of read is within this (or a previous) window
	 */
	if(bamAlignment.Position >= end) return true;

	//find first position to be within window
	double len = bamAlignment.AlignedBases.length();
	if(bamAlignment.Position + len < start) return false;

	//find which position to consider first
	++numReadsInWindow;
	int firstPos = start - bamAlignment.Position;
	if(firstPos < 0) firstPos = 0;
	int lastPos = len;
	if(bamAlignment.Position + lastPos > end) lastPos = end - bamAlignment.Position;

	//add sites
	int internalPos = bamAlignment.Position + firstPos - start;
	char base;
	char quality;

	/* Note:
	 *  1) Reference is 5' -> 3'
	 *  2) distance is 1-based!
	 *  3) Ignoring indels when calculating distances
	 *  4) Function add needs first P(C->T), then P(G->A)
	 */

	for(int pos = firstPos; pos < lastPos; ++pos, ++internalPos){
		base = bamAlignment.AlignedBases.at(pos);
		if(base == 'A' || base == 'C' || base == 'G' || base == 'T'){ //skip any other
			quality = bamAlignment.AlignedQualities.at(pos);
			if(minQuality <= (int) quality && (int) quality <= maxQuality){ //skip if quality does not make sense
				sites[internalPos].add(base, quality, maxNumBasesPerSite);
			}
		}
	}
	//return if part of the read maps to next window
	if(lastPos == len) return false;
	else return true;
}

void TWindow::addReferenceBaseToSites(BamTools::Fasta & reference, int & refId){
	int stop = end - 1; //note that end is last position + 1
	std::string ref; //fasta object fills string
	reference.GetSequence(refId, start, stop, ref);
	for(int i=0; i<length; ++i){
//		if(sites[i].hasData)
		sites[i].setRefBase(ref[i]);
	}
}

void TWindow::addReferenceBaseToSites(TSiteSubset* subset){
	if(subset->hasPositionsInWindow(start)){
		//now only run over sites listed in that window
		std::map<long,std::pair<char,char> > thesePos = subset->getPositionInWindow(start);
		int pos;
		for(std::map<long,std::pair<char,char> >::iterator it=thesePos.begin(); it!=thesePos.end(); ++it){
			pos = it->first - start;
			sites[pos].setRefBase(it->second.first);
		}
	}
}

void TWindow::applyMask(TBedReader* mask){
	//test if mask is required
	if(mask->hasPositionsInWindow(start)){
		//skip sites listed in mask by setting their hasData = false
		std::vector<long> thesePos = mask->getPositionInWindow(start);
		int pos;
		for(std::vector<long>::iterator it=thesePos.begin(); it!=thesePos.end(); ++it){
			pos = *it - start;
			if(pos < length) sites[pos].clear();
		}
	}
}

void TWindow::maskCpG(BamTools::Fasta & reference, int & refId){
	std::string ref; //fasta object fills string
	//note that end is last position + 1
	for(int i=0; i<length; ++i){
		if(ref[i+1] == 'C' && ref[i+2] == 'G') sites[i].clear();
		else if(ref[i] == 'C' && ref[i+1] == 'G') sites[i].clear();
	}
}

void TWindow::estimateBaseFrequencies(){
	//estimate initial base frequencies
	baseFreq.clear();
	for(int i=0; i<length; ++i){
		if(sites[i].hasData){
			sites[i].addToBaseFrequencies(baseFreq);
		}
	}
	baseFreq.normalize();
}

void TWindow::printPileup(std::ofstream & out, int & pos){
	out << "\t" << sites[pos].bases.size() << "\t" << sites[pos].getBases();
}

void TWindow::calcCoverage(){
	//calculate and return coverage
	coverage = 0.0;
	long noData = 0;
	long plentyData = 0;
	for(int i=0; i<length; ++i){
		coverage += sites[i].bases.size();
		if(sites[i].bases.size() == 0) ++noData;
		else if(sites[i].bases.size() > 1) ++ plentyData;
	}

	if(noData >= length)
		coverageAtSitesWithData = 0.0;
	else
		coverageAtSitesWithData = coverage / (double) (length - noData);
	coverage = coverage / (double) length;
	fractionSitesNoData = (double) noData / (double) length;
	fractionsitesCoverageAtLeastTwo = (double) plentyData / (double) length;
}

void TWindow::calcCoveragePerSite(long * siteCoverage, unsigned int maxCov){
	//calculate and return coverage
	coverage = 0.0;
	long noData = 0;
	long plentyData = 0;

	for(int i=0; i<length; ++i){
		if(sites[i].bases.size() <= maxCov)	siteCoverage[sites[i].bases.size()] += 1;
		else siteCoverage[maxCov + 1] += 1; //else it should be in the "greater than" bin

		if(sites[i].bases.size() == 0) ++ noData;
		else if(sites[i].bases.size() > 1) ++ plentyData;
	}

	coverage = coverage / (double) length;
	fractionSitesNoData = (double) noData / (double) length;
	fractionsitesCoverageAtLeastTwo = (double) plentyData / (double) length;
}

void TWindow::applyCoverageFilter(int minCoverage, int maxCoverage){
	for(int i=0; i<length; ++i){
		if(sites[i].hasData){
			if(sites[i].bases.size() < minCoverage || sites[i].bases.size() > maxCoverage)
				sites[i].clear();
		}
	}
}

void TWindow::fillBaseCounts(int** counts){
	for(int i=0; i<length; ++i){
		sites[i].fillBaseCounts(counts[i]);
	}
}

//-------------------------------------
//AFL functions
//-------------------------------------
void TWindow::deleteAlleleFrequencyLikelihoods(){
	if(alleleFrequencyLikelihoodsInitialized){
		for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
			for(int i=0; i<length; ++i){
				delete[] alleleFrequencyLikelihoods[c][i];
			}
			delete[] alleleFrequencyLikelihoods[c];
		}
		delete[] alleleFrequencyLikelihoods;
		alleleFrequencyLikelihoodsInitialized = false;
	}

	//allele frequency estimates
	if(alleleFrequencyEstimatesInitialized){
		for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
			delete[] MLE_AlleleFrequencies[c];
			delete[] MLE_AlleleFrequencies_LL[c];
		}
		delete[] MLE_AlleleFrequencies;
		delete[] MLE_AlleleFrequencies_LL;
		for(int c=0; c<4; ++c)
			delete[] MLE_AlleleFrequencies_LL_nullModel[c];
		delete[] MLE_AlleleFrequencies_LL_nullModel;
		alleleFrequencyEstimatesInitialized = false;
	}
}

void TWindow::initializeAlleleFrequencyLikelihoods(int & NumChromosomes){
	if(!alleleFrequencyLikelihoodsInitialized){
		numChromosomes = NumChromosomes;
		//for all combination of alleles AC, AG, AT, CG, CT, GT, ...
		alleleFrequencyLikelihoods = new float**[genoMap.numHeterozygousGenotypes];

		MLE_AlleleFrequencyIndex = new int*[genoMap.numHeterozygousGenotypes];
		for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
			alleleFrequencyLikelihoods[c] = new float*[length];
			MLE_AlleleFrequencyIndex[c] = new int[length];
			for(int i=0; i<length; ++i){
				alleleFrequencyLikelihoods[c][i] = new float[numChromosomes+1];
			}
		}
		alleleFrequencyLikelihoodsInitialized = true;
	} else if(numChromosomes!= NumChromosomes){
		for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
			for(int i=0; i<length; ++i){
				delete[] alleleFrequencyLikelihoods[c][i];
				alleleFrequencyLikelihoods[c][i] = new float[numChromosomes+1];
			}
		}
	}
}

void TWindow::calculateAlleleFreqLikelihoods(int & NumChromosomes){
	if(!alleleFrequencyLikelihoodsCalculated){
		initializeAlleleFrequencyLikelihoods(NumChromosomes);
		//calculate for all allele combinations

#pragma omp paralell for
		for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
			std::pair<Base,Base> alleles = genoMap.getBasesOfHeterozygousGenotype(c);
			for(int i=0; i<length; ++i){
				sites[i].calculateAlleleFrequencyLikelihoods(numChromosomes, alleles.first, alleles.second, alleleFrequencyLikelihoods[c][i], genoMap);
			}
		}
		alleleFrequencyLikelihoodsCalculated = true;
	}
}

void TWindow::findMLAlleleFrequencies(){
	if(!alleleFrequencyLikelihoodsCalculated)
		throw "Can not find MLE: allele frequency likelihoods have to be calculated first!";

	//now find MLE for each alleleic combination
	int mle;
	for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
		for(int i=0; i<length; ++i){
			mle = 0;
			for(int y = 1; y < (numChromosomes + 1); ++y){
				if(alleleFrequencyLikelihoods[c][i][y] > alleleFrequencyLikelihoods[c][i][mle]){
					mle = y;
				}
			}
			MLE_AlleleFrequencyIndex[c][i] = mle;
		}
	}
}

double TWindow::getAlleleFrequencyLikelihoodAtMLE(const int & site, const int & alleleicCombination){
	return alleleFrequencyLikelihoods[alleleicCombination][site][MLE_AlleleFrequencyIndex[alleleicCombination][site]];
}

double TWindow::getMLEAlleleFrequency(const int & site, const int & alleleicCombination){
	return (double) MLE_AlleleFrequencyIndex[alleleicCombination][site] / numChromosomes;
}

int TWindow::getMLEAlleleCounts(const int & site, const int & alleleicCombination){
	return MLE_AlleleFrequencyIndex[alleleicCombination][site];
}


double TWindow::getAlleleFrequencyLikleihood_monomorphic(int & site, int & allele){
	//use frequency = 0 bin for the correct combination -> this is a hack!
	if(allele == A) return alleleFrequencyLikelihoods[0][site][numChromosomes];
	if(allele == C) return alleleFrequencyLikelihoods[0][site][0];
	if(allele == G) return alleleFrequencyLikelihoods[1][site][0];
	if(allele == T) return alleleFrequencyLikelihoods[2][site][0];
	return 0.0;
}

void TWindow::addToAlleleFrequencyLikelihoodAtMLE(float** alleleFrequencyLikelihoodAtMLE, float** alleleFrequencyLikelihoodAtMonomorphic){
	for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
		for(int i=0; i<length; ++i){
			alleleFrequencyLikelihoodAtMLE[c][i] += alleleFrequencyLikelihoods[c][i][MLE_AlleleFrequencyIndex[c][i]];
		}
	}
	for(int c=0; c<4; ++c){
		for(int i=0; i<length; ++i){
			alleleFrequencyLikelihoodAtMonomorphic[c][i] += getAlleleFrequencyLikleihood_monomorphic(i, c);
		}
	}
}

void TWindow::writeAlleleFrequencyLikelihoods(gz::ogzstream & outFile, int & alleleicCombination, int & site){
	int y = 0;
	outFile << '\t' << alleleFrequencyLikelihoods[alleleicCombination][site][y];
	for(y = 1; y < (numChromosomes + 1); ++y)
		outFile << '|' << alleleFrequencyLikelihoods[alleleicCombination][site][y];
}

//------------------------------------------------
//functions for pooled data (unknown sample size)
//------------------------------------------------
void TWindow::initializeAlleleFrequencyEstimates(){
	if(!alleleFrequencyEstimatesInitialized){
		//for all combination of alleles AC, AG, AT, CG, CT, GT
		MLE_AlleleFrequencies = new float*[genoMap.numHeterozygousGenotypes];
		MLE_AlleleFrequencies_LL = new float*[genoMap.numHeterozygousGenotypes];
		for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
			MLE_AlleleFrequencies[c] = new float[length];
			MLE_AlleleFrequencies_LL[c] = new float[length];
		}
		MLE_AlleleFrequencies_LL_nullModel = new float*[4];
		for(int c=0; c<4; ++c)
			MLE_AlleleFrequencies_LL_nullModel[c] = new float[length];
		alleleFrequencyEstimatesInitialized = true;
	}
}

void TWindow::findMLEFrequenciesPooled(int & maxNumIterations, float & maxDelta){
	initializeAlleleFrequencyEstimates();

	//some variables
	std::pair<Base,Base> alleles;
	double firstDerivative, secondDerivative;
	double f, new_f;

	//find MLE for each site
	for(int i=0; i<length; ++i){
		if(sites[i].hasData){
			//estimate MLE frequency for each alelleic combination
			for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
				//guess initial frequency
				alleles = genoMap.getBasesOfHeterozygousGenotype(c);
				f = sites[i].getRoughEstimateOfAlleleFrequency(alleles.first, alleles.second);

				//run NEwton-Raphson
				for(int r = 0; r < maxNumIterations; ++r){
					//calculate derivatives
					sites[i].calculatePooledLogLikelihoodDerivatives(f, alleles.first, alleles.second, firstDerivative, secondDerivative);

					//update f
					new_f = f - firstDerivative / secondDerivative;

					//decide whether to stop Newton-Raphson
					if(fabs(new_f - f) < maxDelta)
						break;
				}

				//calc likelihood and save
				MLE_AlleleFrequencies[c][i] = f;
				MLE_AlleleFrequencies_LL[c][i] = sites[i].calculatePooledLogLikelihood(f, alleles.first, alleles.second);
			}

			//calculate for Null models
			MLE_AlleleFrequencies_LL_nullModel[A][i] = sites[i].calculatePooledLogLikelihoodNullModel(A);
			MLE_AlleleFrequencies_LL_nullModel[C][i] = sites[i].calculatePooledLogLikelihoodNullModel(C);
			MLE_AlleleFrequencies_LL_nullModel[G][i] = sites[i].calculatePooledLogLikelihoodNullModel(G);
			MLE_AlleleFrequencies_LL_nullModel[T][i] = sites[i].calculatePooledLogLikelihoodNullModel(T);

		} else {
			for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
				MLE_AlleleFrequencies[c][i] = -1.0;
				MLE_AlleleFrequencies_LL[c][i] = 0.0;
			}
			MLE_AlleleFrequencies_LL_nullModel[A][i] = 0.0;
			MLE_AlleleFrequencies_LL_nullModel[C][i] = 0.0;
			MLE_AlleleFrequencies_LL_nullModel[G][i] = 0.0;
			MLE_AlleleFrequencies_LL_nullModel[T][i] = 0.0;
		}
	}
}

void TWindow::addToPooledFrequencyLikelihoodAtMLE(float** likelihoodAtMLE, float** likelihoodAtMonomorphic){
	for(int c=0; c<genoMap.numHeterozygousGenotypes; ++c){
		for(int i=0; i<length; ++i){
			likelihoodAtMLE[c][i] += MLE_AlleleFrequencies_LL[c][i];
		}

	}
	for(int c=0; c<4; ++c){
		for(int i=0; i<length; ++i){
			likelihoodAtMonomorphic[c][i] += MLE_AlleleFrequencies_LL_nullModel[c][i];
		}

	}
}

double TWindow::getMLEAlleleFrequencyPooled(int & site, int & alleleicCombination){
	if(sites[site].hasData)
		return MLE_AlleleFrequencies[alleleicCombination][site];
	else
		return -1.0;
}


