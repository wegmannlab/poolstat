/*
 * TBamFile.cpp
 *
 *  Created on: Nov 18, 2016
 *      Author: phaentu
 */


#include "TBamFile.h"

//---------------------------------------------------------------
//TMask
//---------------------------------------------------------------
TReferenceAndMask::TReferenceAndMask(TLog* logfile, TParameters & params, int windowSize){
	//open FASTA reference
	if(params.parameterExists("fasta")){
		std::string fastaFile = params.getParameterString("fasta");
		std::string fastaIndex = fastaFile + ".fai";
		logfile->list("Reading reference sequence from '" + fastaFile + "'");
		if(!reference.Open(fastaFile, fastaIndex)) throw "Failed to open FASTA file '" + fastaFile + "'! Is index file present?";
		fastaReference = true;
	} else fastaReference = false;

	//check if we mask sites
	if(params.parameterExists("mask")){
		doMasking = true;
		std::string maskFile = params.getParameterString("mask");
		logfile->startIndent("Will mask all sites listed in BED file '" + maskFile + "':");
		logfile->listFlush("Reading file ...");
		mask = new TBedReader(maskFile, windowSize);
		logfile->write(" done!");
		logfile->endIndent();
	} else doMasking = false;

	if(params.parameterExists("maskCpG")){
		if(!fastaReference) throw "Cannot mask CpG sites without reference!";
		doCpGMasking = true;
		std::string maskFile = params.getParameterString("maskCpG");
		logfile->list("Will mask all CpG sites");
	} else doCpGMasking = false;

	if(params.parameterExists("minCoverage") || params.parameterExists("maxCoverage")){
		applyCoverageFilter = true;
		minCoverage = params.getParameterIntWithDefault("minCoverage", 0);
		maxCoverage = params.getParameterIntWithDefault("maxCoverage", 1000000);
		logfile->list("Will filter out sites with coverage < " + toString(minCoverage) + " or > " + toString(maxCoverage));
	} else {
		applyCoverageFilter = false;
		minCoverage = 0;
		maxCoverage = 1000000;
	}

	if(params.parameterExists("minQual") || params.parameterExists("maxQual")){
		applyQualityFilter = true;
		minQuality = params.getParameterInt("minQual") + 33;
		maxQuality = params.getParameterInt("maxQual") + 33;
		logfile->list("Will filter out bases with quality <= " + toString(minQuality-33) + " or >= " + toString(maxQuality-33));
	} else {
		applyQualityFilter = false;
		minQuality = 33;
		maxQuality = 1000000;
	}

	maxNumBasesReadPerSite = params.getParameterIntWithDefault("maxReadDepth", 8000);
	logfile->list("Will only read the first " + toString(maxNumBasesReadPerSite) + " bases for each site. Additional bases will be ignored.");
}

void TReferenceAndMask::setChr(std::string & chr){
	if(doMasking) mask->setChr(chr);
}

void TReferenceAndMask::fillReference(int refID, int windowStart, int windowEnd){
	//note that window end is last position + 1
	int stop = windowEnd - 1;
	reference.GetSequence(refID, windowStart, stop, referenceForWindow);
}

//---------------------------------------------------------------
//TBamFile
//---------------------------------------------------------------
TBamFile::TBamFile(std::string Filename, const int & WindowSize, TReferenceAndMask* RefAndMask, TLog* Logfile){
	openBamFile(Filename, WindowSize, refAndMask, Logfile);
	numChromosomes = -1;
	numchromosomesKnown = false;
}

void TBamFile::openBamFile(std::string Filename, const int & WindowSize, TReferenceAndMask* RefAndMask, TLog* Logfile){
	filename = Filename;
	filenameTag = extractBeforeLast(Filename, '.');
	windowSize = WindowSize;
	refAndMask = RefAndMask;
	logfile = Logfile;

	//open bam reader
	if (!bamReader.Open(filename))
		throw "Failed to open BAM file '" + filename + "'!";

	//load index file
	if(!bamReader.LocateIndex())
		throw "No index file found for BAM file '" + filename + "'!";

	//open header
	bamHeader = bamReader.GetHeader();
	readGroups.fill(bamHeader);

	//other stuff
	oldAlignementMustBeConsidered = false;
	oldPos = -1;
}

void TBamFile::setNumChromosomes(int NumChromosomes){
	numChromosomes = NumChromosomes;
	numchromosomesKnown = true;
}

void TBamFile::jumpToEnd(){
	chrIterator = bamHeader.Sequences.End();
	chrNumber = -1;
}

bool TBamFile::setChr(std::string & name){
	chrNumber = 0;
	for(chrIterator = bamHeader.Sequences.Begin(); chrIterator != bamHeader.Sequences.End(); ++chrIterator, ++chrNumber){
		if(chrIterator->Name == name) break;
	}
	if(chrIterator == bamHeader.Sequences.End()) return false;

	//jump reader
	bamReader.Jump(chrNumber, 0);
	oldAlignementMustBeConsidered = false;

	//restart windows
	chrLength = stringToLong(chrIterator->Length);

	curStart = 0;
	curEnd = 0;
	oldPos = -1;
	windowNumber = 0;
	int nextEnd = windowSize;
	if(nextEnd > chrLength) nextEnd = chrLength;
	windowPair.nextPointer->move(0, nextEnd);
	return true;
}

void TBamFile::fillVectorOfChromosomes(std::vector<std::string> & vec){
	vec.clear();
	for(chrIterator = bamHeader.Sequences.Begin(); chrIterator != bamHeader.Sequences.End(); ++chrIterator, ++chrNumber)
		vec.push_back(chrIterator->Name);
}

long TBamFile::iterateWindow(){
	//swap window pairs
	windowPair.swap();

	//move to next region
	curStart = curEnd;
	curEnd = windowPair.curPointer->end;
	if(curStart >= chrLength) return -1;
	curWindowSize = curEnd - curStart;

	//move next
	long nextEnd = curEnd + windowSize;
	if(nextEnd > chrLength) nextEnd = chrLength + 1;
	windowPair.nextPointer->move(curEnd, nextEnd);

	++windowNumber;

	//jump reader
	bamReader.Jump(chrNumber, curStart);

	return curStart;
};

bool TBamFile::addAlignementToWindows(BamTools::BamAlignment & alignement){
	//std::cout << "REF ID = " << bamAlignement.RefID << "\tpos = " << bamAlignement.Position << std::endl;
	//only take those reads that pass QC
	if(!alignement.IsFailedQC() && !alignement.IsDuplicate() && alignement.Position >= curStart){
		//check if bam file is sorted
		if(alignement.Position < oldPos){
			throw "BAM file '" + filename + "' must be sorted by position!";
		}
		oldPos = alignement.Position;

		//check if still within current window and add to window
		if(alignement.Position >= curEnd) return false;
		else {
			if(windowPair.curPointer->addFromRead(alignement, refAndMask->minQuality, refAndMask->maxQuality, refAndMask->maxNumBasesReadPerSite)){
				//add also to next window in case reads overhangs current window -> function returns true
				windowPair.nextPointer->addFromRead(alignement, refAndMask->minQuality, refAndMask->maxQuality, refAndMask->maxNumBasesReadPerSite);
			}
		}
	}
	return true; //continue
}

bool TBamFile::readData(){
	logfile->listFlush("Reading data from '" + filename + " ...");

	//measure runtime
	struct timeval start, end;
	gettimeofday(&start, NULL);

	//parse through reads
	if(oldAlignementMustBeConsidered){
		oldAlignementMustBeConsidered = false;
		if(!addAlignementToWindows(bamAlignment)){
			//next read is for a later window
			oldAlignementMustBeConsidered = true;
			gettimeofday(&end, NULL);
			logfile->write(" done (in " , end.tv_sec  - start.tv_sec, "s)!");
			logfile->conclude("No data in this window.");
			return false; //still only in next window
		}
	}

	while(bamReader.GetNextAlignment(bamAlignment) && bamAlignment.RefID==chrNumber){
		if(readGroups.readGroupInUse(bamAlignment)){
			//filter out unmapped reads and those that did not pass QC
			if(bamAlignment.IsMapped() && !bamAlignment.IsFailedQC()){
				//check if read is paired and reject reads with pairs on different chromosomes (maybe too harsh?)
				//if(!bamAlignment.IsPaired() || bamAlignment.MateRefID == bamAlignment.RefID){
					//check if insert size is shorter than read, this means we are reading the adaptor sequence
					if(bamAlignment.IsPaired() && bamAlignment.IsMateMapped() && abs(bamAlignment.InsertSize) < bamAlignment.AlignedBases.length())
						logfile->warning("The alignment " + bamAlignment.Name + " is longer than its insert size of " + toString(abs(bamAlignment.InsertSize)));

					if(!addAlignementToWindows(bamAlignment)){
						//read is beyond window and should be reconsidered
						oldAlignementMustBeConsidered = true;
						break;
					}

				//}
			}
		}
	}

	gettimeofday(&end, NULL);
	logfile->write(" done (in " , end.tv_sec  - start.tv_sec, "s)!");

	if(windowPair.curPointer->numReadsInWindow > 0){
		//apply masks and filters
		if(refAndMask->doMasking){
			logfile->listFlush("Masking sites ...");
			windowPair.curPointer->applyMask(refAndMask->mask);
			logfile->write(" done!");
		}
		if(refAndMask->doCpGMasking){
			logfile->listFlush("Masking CpG sites ...");
			windowPair.curPointer->maskCpG(refAndMask->reference, chrNumber);
			logfile->write(" done!");
		}
		if(refAndMask->applyCoverageFilter){
			windowPair.curPointer->applyCoverageFilter(refAndMask->minCoverage, refAndMask->maxCoverage);
		}

		//calc coverage
		windowPair.curPointer->calcCoverage();

		//report
		logfile->conclude("read data from " + toString(windowPair.curPointer->numReadsInWindow) + " reads.");
		logfile->conclude("average coverage is " + toString(windowPair.curPointer->coverage));
		logfile->conclude(toString(windowPair.curPointer->fractionSitesNoData * 100) + "% of all sites have no data");
		if(windowPair.curPointer->fractionSitesNoData < 1.0)
			logfile->conclude("average coverage at sites with data is " + toString(windowPair.curPointer->coverageAtSitesWithData));
		return true;
	} else {
		logfile->conclude("No data in this window.");
		return false;
	}
};

long TBamFile::getCurPos(int & posInWindow){
	return curStart + posInWindow;
};

void TBamFile::printPileup(std::ofstream & out, int & posInWindow){
	windowPair.curPointer->printPileup(out, posInWindow);
};

void TBamFile::fillBaseCounts(int** counts){
	windowPair.curPointer->fillBaseCounts(counts);
};

void TBamFile::calcAlleleFrequencyLikelihoods(){
	windowPair.curPointer->calculateAlleleFreqLikelihoods(numChromosomes);
}

void TBamFile::addToAlleleFrequencyLikelihoodAtMLE(float** alleleFrequencyLikelihoodAtMLE, float** alleleFrequencyLikelihoodAtMonomorphic){
	windowPair.curPointer->findMLAlleleFrequencies();
	windowPair.curPointer->addToAlleleFrequencyLikelihoodAtMLE(alleleFrequencyLikelihoodAtMLE, alleleFrequencyLikelihoodAtMonomorphic);
}

void TBamFile::writeAlleleFrequencyLikelihoodHeader(gz::ogzstream & outFile, std::string name){
	for(int i=0; i<=numChromosomes; ++i)
		outFile << name << i;
}

void TBamFile::writeAlleleFrequencyLikelihoods(gz::ogzstream & outFile, int site, int alleleicCombination){
	windowPair.curPointer->writeAlleleFrequencyLikelihoods(outFile, alleleicCombination, site);
}

void TBamFile::writeMLEAlleleFrequency(gz::ogzstream & outFile, int site, int alleleicCombination){
	if(windowPair.curPointer->siteHasData(site))
		outFile << "\t" << windowPair.curPointer->getMLEAlleleFrequency(site, alleleicCombination);
	else outFile << "\t-";
}

void TBamFile::writeMLEAlleleFrequencyFlip(gz::ogzstream & outFile, int site, int alleleicCombination){
	if(windowPair.curPointer->siteHasData(site))
		outFile << "\t" << 1.0 - windowPair.curPointer->getMLEAlleleFrequency(site, alleleicCombination);
	else outFile << "\t-";
}

double TBamFile::getMLEAlleleFrequency(const int & site, const int & alleleicCombination){
	return windowPair.curPointer->getMLEAlleleFrequency(site, alleleicCombination);
}

double TBamFile::getMLEAlleleFrequencyFlip(const int & site, const int & alleleicCombination){
	return 1.0 - windowPair.curPointer->getMLEAlleleFrequency(site, alleleicCombination);
}

int TBamFile::getMLEAlleleCounts(const int & site, const int & alleleicCombination){
	return windowPair.curPointer->getMLEAlleleCounts(site, alleleicCombination);
}

void TBamFile::addToPooledMLEAlleleFrequency(int & site, int & alleleicCombination, float & freq, float & nChr){
	if(windowPair.curPointer->siteHasData(site)){
		freq += windowPair.curPointer->MLE_AlleleFrequencyIndex[alleleicCombination][site];
		nChr += numChromosomes;
	}
}

void TBamFile::openSafFile(){
	safFile = new TSafFile(filenameTag, numChromosomes);
	safFileOpend = true;
}

void TBamFile::setSmallestAFLToPrint(float value){
	if(!safFileOpend)
		throw "Can not set smallest AFL to print: SAF file has not been open yet!";
	safFile->setSmallestAFLToPrint(value);
}

void TBamFile::writeAlleleFrequencyLikelihoodsToSafFile(std::string & chr, int site, int alleleicCombination){
	if(windowPair.curPointer->siteHasData(site))
		safFile->writeLogAFLs(chr, curStart + site, windowPair.curPointer->pointerToAlleleFrequencyLikleihoods(alleleicCombination, site));
}

void TBamFile::writeAlleleFrequencyLikelihoodsToSafFileFlipOrder(std::string & chr, int site, int alleleicCombination){
	if(windowPair.curPointer->siteHasData(site)){
		float* afl = windowPair.curPointer->pointerToAlleleFrequencyLikleihoods(alleleicCombination, site);
		float* tmp = new float[windowPair.curPointer->numChromosomes + 1];
		for(int i=0; i<numChromosomes+1; ++i)
			tmp[i] = afl[numChromosomes - i];
		safFile->writeLogAFLs(chr, curStart + site, tmp);
	}
}

void TBamFile::writeMLEAlleleCountToApproxWFFile(gz::ogzstream & out, const int & site, const int & alleleicCombination){
	out << "\t" << windowPair.curPointer->getMLEAlleleCounts(site, alleleicCombination) << "/" << windowPair.curPointer->numChromosomes;
}

//pooled data
void TBamFile::findMLEFrequenciesPooled(int & maxNumIterations, float & maxDeltaF){
	windowPair.curPointer->findMLEFrequenciesPooled(maxNumIterations, maxDeltaF);
}

void TBamFile::addToPooledFrequencyLikelihoodAtMLE(float** likelihoodAtMLE, float** likelihoodAtMonomorphic){
	windowPair.curPointer->addToPooledFrequencyLikelihoodAtMLE(likelihoodAtMLE, likelihoodAtMonomorphic);
}

float TBamFile::getMLEAlleleFrequencyPooled(int site, int alleleicCombination){
	return windowPair.curPointer->getMLEAlleleFrequencyPooled(site, alleleicCombination);
}

