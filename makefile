#make file for atlas

CC=g++

SRC = $(wildcard *.cpp) $(wildcard *.C) $(wildcard bamtools/api/*.cpp) $(wildcard bamtools/api/algorithms/*.cpp) $(wildcard bamtools/api/internal/bam/*.cpp) $(wildcard bamtools/api/internal/index/*.cpp) $(wildcard bamtools/api/internal/io/*.cpp) $(wildcard bamtools/api/internal/sam/*.cpp) $(wildcard bamtools/api/internal/utils/*.cpp) $(wildcard bamtools/utils/*.cpp) 

OBJ = $(SRC:%.cpp=%.o)

BIN = poolStat

all:	$(BIN)

$(BIN):	$(OBJ)
	$(CC) -O3 -o $(BIN) $(OBJ) -lz -lhts
	#If you do not have a systemwide installation of htslib, you need to provide the folder to htslin with comman -L as follows:
	$(CC) -O3 -o $(BIN) $(OBJ) HTSLIBFOLDER/libhts.a -lz -lm -llzma -pthread -lcurl -lcrypto -lbz2
	#where HTSLIBFOLDER is the path of the folder where you a compiled version of htslib available.
	#You will also need to change the line below accordingly.
	

%.o: %.cpp
	$(CC) -O3 -c -Ibamtools -std=c++1y $< -o $@	
	#Use the following line if you do not have a system wide installation of htslib (see also above)
	$(CC) -O3 -c -Ibamtools -IHTSLIBFOLDER -std=c++1y $< -o $@	

clean:
	rm -rf *.o poolStat
