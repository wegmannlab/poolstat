/*
 * estimHet.cpp
 *
 *  Created on: Feb 19, 2015
 *      Author: wegmannd
 */

#include "TLog.h"
#include "TParameters.h"
#include "TGenome.h"
#include "TSimulator.h"
#include "stringFunctions.h"

void simulatePooledData(TLog* logfile, TParameters & params){
	//initialize random generator
	TRandomGenerator* randomGenerator;
	logfile->listFlush("Initializing random generator ...");
	if(params.parameterExists("fixedSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("fixedSeed"), true);
	} else if(params.parameterExists("addToSeed")){
		randomGenerator=new TRandomGenerator(params.getParameterLong("addToSeed"), false);
	} else randomGenerator=new TRandomGenerator();
	logfile->write(" done with seed " + toString(randomGenerator->usedSeed) + "!");

	//initialize simulator
	TSimulator simulator(logfile, randomGenerator);

	//read parameters
	logfile->startIndent("Reading simulation parameters:");
	std::string outname = params.getParameterStringWithDefault("outname", "poolstat_simulation");
	logfile->list("Will write output files with tag '" + outname + "'.");

	//prepare SFS
	SFS* sfs;
	int sampleSize;
	logfile->startIndent("Preparing SFS:");
	if(params.parameterExists("sfs")){
		std::string sfsFile = params.getParameterString("sfs");
		logfile->listFlush("Reading the sfs from file '" + sfsFile + "' ...");
		sfs = new SFS(sfsFile);
		sampleSize = sfs->numChromosomes;
		logfile->done();
	} else if(params.parameterExists("oneBin")){
		sampleSize = params.getParameterInt("sampleSize");
		logfile->list("Will simulate data from " + toString(sampleSize) + " pooled chromosomes.");
		int oneBin = params.getParameterDouble("oneBin");
		logfile->listFlush("Generating SFS for " + toString(sampleSize) + " chromosomes with all entries in bin " + toString(oneBin) + " ...");
		sfs = new SFS(sampleSize, oneBin);
		logfile->done();
	} else if(params.parameterExists("theta")){
		sampleSize = params.getParameterInt("sampleSize");
		logfile->list("Will simulate data from " + toString(sampleSize) + " pooled chromosomes.");
		float theta = params.getParameterDouble("theta");
		logfile->listFlush("Generating SFS for " + toString(sampleSize) + " chromosomes and with theta = " + toString(theta) + " ...");
		sfs = new SFS(sampleSize, theta);
		logfile->done();
	} else throw "Specifying either sfs, theta or oneBin is required to initialize the SFS!";
	std::string filename = outname + "_trueSFS.txt";
	logfile->listFlush("Writing true SFS to '" + filename + "' ...");
	sfs->writeToFile(filename);
	logfile->done();
	//also write on log scale
	filename = outname + "_trueSFS_log.txt";
	logfile->listFlush("Writing true SFS on log scale to '" + filename + "' ...");
	sfs->writeToFile(filename, true);
	logfile->done();
	logfile->endIndent();


	//read length & depth
	float depth = params.getParameterDoubleWithDefault("depth", 10.0);
	simulator.setDepth(depth);
	int readLength = params.getParameterIntWithDefault("readLength", 100);
	simulator.setReadLength(readLength);
	logfile->list("Will simulate reads of length " + toString(readLength) + " to a depth of " + toString(depth) + ".");

	//chromosomes
	int numChr = params.getParameterIntWithDefault("numChr", 1);
	long chrLength = params.getParameterLongWithDefault("chrLength", 1000000);
	logfile->list("Will simulate " + toString(numChr) + " of length " + toString(chrLength) + " each.");
	simulator.initializeChromosomes(numChr, chrLength);

	//quality distribution
	double meanQual = params.getParameterDoubleWithDefault("meanQual", 30);
	double sdQual = params.getParameterDoubleWithDefault("sdQual", 10);
	logfile->list("Will simulate normal distributed quality scores with mean = " + toString(meanQual) + " and sd = " + toString(sdQual));
	simulator.setQualityDistribution(meanQual, sdQual);
	logfile->endIndent();

	//now run simulation!
	simulator.simulatePooledData(sampleSize, *sfs, outname);

	//clean up
	delete randomGenerator;
	delete sfs;
}

void printSafFile(TLog* logfile, TParameters & params){
	//open SAF file
	std::string safFile = params.getParameterString("saf");
	logfile->list("Will parse site allele frequency likelihoods from file '" + safFile + "'.");
	logfile->listFlush("Opening SAF file for reading ...");
	TSafFileReader saf(safFile);
	logfile->done();
	logfile->conclude("Will read SAF file of version " + saf.version());
	logfile->conclude("SAF file contains data for " + toString(saf.sampleSize()) + " chromosomes.");

	//limit sites
	bool limitSites = false;
	long maxNumSites = 0;
	if(params.parameterExists("limitSites")){
		limitSites = true;
		maxNumSites = params.getParameterLong("limitSites");
		if(maxNumSites <=0)
			throw "Can not limit to less than one site!";
	}

	//parse SAF file
	long numLines = 0;
	logfile->startIndent("Printing SAF file:");
	while(saf.readLine()){
		saf.printLine();

		//do we limit sites?
		++numLines;
		if(limitSites && numLines >= maxNumSites)
			break;
	}
}

void estimateSFS(TLog* logfile, TParameters & params){
	//open SAF file
	std::string safFile = params.getParameterString("saf");
	logfile->list("Will parse site allele frequency likelihoods from file '" + safFile);
	logfile->listFlush("Opening SAF file for reading ...");
	TSafFileReader saf(safFile);
	logfile->done();
	logfile->conclude("Will read SAF file of version " + saf.version());
	logfile->conclude("SAF file contains data for " + toString(saf.sampleSize()) + " chromosomes.");

	std::string outputName = params.getParameterStringWithDefault("out", "");
	if(outputName == ""){
		//guess from filename
		outputName = safFile;
		outputName = extractBeforeLast(outputName, ".");
	}
	logfile->list("Writing output files with prefix '" + outputName + "'.");

	//limit sites
	bool limitSites = false;
	long maxNumSites = 0;
	if(params.parameterExists("limitSites")){
		limitSites = true;
		maxNumSites = params.getParameterLong("limitSites");
		if(maxNumSites <=0)
			throw "Can not limit to less than one site!";
	}

	//prepare storage
	int dimension = saf.dimensionality();
	double* sum = new double[dimension];

	//parse SAF file
	long numLines = 0;
	struct timeval start, end;
	gettimeofday(&start, NULL);
	logfile->startIndent("Parsing SAF file:");
	while(saf.readLine()){
		for(int i=0; i<dimension; ++i){
			sum[i] += exp(saf.likelihoods[i]);
		}

		//progress
		++numLines;
		if(numLines % 1000000 == 0){
			gettimeofday(&end, NULL);
			float runtime=(end.tv_sec  - start.tv_sec)/60.0;
			logfile->list("Parsed " + toString(numLines) + " sites in " + toString(runtime) + " min ...");
		}

		//do we limit sites?
		if(limitSites && numLines >= maxNumSites)
			break;
	}

	//report progress at end
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile->list("Parsed " + toString(numLines) + " sites in " + toString(runtime) + " min ...");
	if(!limitSites || numLines < maxNumSites)
		logfile->list("Reached end of SAf file");
	logfile->endIndent();

	//normalize SFS
	double tot = 0.0;
	for(int i=0; i<dimension; ++i){
		tot += sum[i];
	}
	for(int i=0; i<dimension; ++i){
		sum[i] /= tot;
	}

	//printing / writing SFS
	std::string filename = outputName + ".sfs";
	logfile->listFlush("Writing SFS to file '" + filename + "' ...");
	std::ofstream out(filename.c_str());
	if(!out)
		throw "Failed to open output file '" + filename + "'!";

	for(int i=0; i<dimension; ++i){
		if(i>0) out << " ";
		out << sum[i];
	}
	out.close();
	logfile->done();

	logfile->startIndent("Estimated SFS:");
	for(int i=0; i<dimension; ++i){
		logfile->list(sum[i]);
	}
}


void calcSfsLL(TLog* logfile, TParameters & params){
	//calculate the log-likelihood of an SFS using the allele frequency likelihoods from a saf file
	//read SFS
	std::string sfsFilename = params.getParameterString("sfs");
	logfile->listFlush("Reading SFS from file '" + sfsFilename + " ...");
	SFS origSfs(sfsFilename);
	logfile->done();
	logfile->conclude("Read an SFS for " + toString(origSfs.numChromosomes) + " chromosomes");
	int numSfs = 1;

	std::string outputName = params.getParameterStringWithDefault("out", "");
	if(outputName == ""){
		//guess from filename
		outputName = params.getParameterString("sfs");
		outputName = extractBeforeLast(outputName, ".");
	}
	logfile->list("Writing output files with prefix '" + outputName + "'.");


	//open SAF file
	std::string safFile = params.getParameterString("saf");
	logfile->list("Will parse site allele frequency likelihoods from file '" + safFile);
	logfile->listFlush("Opening SAF file for reading ...");
	TSafFileReader saf(safFile);
	logfile->done();
	logfile->conclude("Will read SAF file of version " + saf.version() + ".");
	logfile->conclude("SAF file contains data for " + toString(saf.sampleSize()) + " chromosomes.");

	if(origSfs.numChromosomes != saf.sampleSize())
		throw "SFS and SAF file have different dimensionality!";

	//do we check rescaling of zero bin?
	std::vector<float> monoFrac;
	SFS** allSfs;
	if(params.parameterExists("monoFrac")){
		std::string monoFracString = params.getParameterString("monoFrac");
		//is it a range?
		if(monoFracString.find('-')!=std::string::npos){
			std::vector<std::string> s;
			logfile->list("Interpreting the monoFrac string '" + monoFracString + "' as a range.");
			if(monoFracString.find(':') == std::string::npos) throw "Wrong format! Use 'from-to:num' format.";
			fillVectorFromString(monoFracString, s, ':');
			if(s.size()!=2) throw "Wrong format! Use 'from-to:num' format.";
			int numValsInRange = stringToInt(s[1]);
			if(numValsInRange<2) throw "A range must consist of at least 2 values!";
			fillVectorFromString(s[0], s, '-');
			if(s.size()!=2) throw "Wrong format! Use 'from-to:num' format.";
			float minMonoFrac=stringToDouble(s[0]);
			float maxMonoFrac=stringToDouble(s[1]);
			if(minMonoFrac > maxMonoFrac){
				float temp = maxMonoFrac;
				maxMonoFrac = minMonoFrac;
				minMonoFrac = temp;
			}
			if(minMonoFrac < 0.0) throw "Minimum monoFrac has to be >= 0.0!";
			if(minMonoFrac > 1.0) throw "Maximum monoFrac has to be <= 1.0!";

			//now generate values
			float step = (maxMonoFrac-minMonoFrac)/(numValsInRange-1);
			for(int i=0; i<numValsInRange; ++i)
				monoFrac.push_back(minMonoFrac + i*step);
		} else {
			fillVectorFromString(monoFracString, monoFrac, ',');
		}
		if(monoFrac.size() > 0){
			numSfs = monoFrac.size() + 1;
			allSfs = new SFS*[numSfs];
			allSfs[0] = &origSfs;
			logfile->listFlush("Will also test SFS rescaled for the following fraction of monomorphic sites:");
			std::string sep = " ";
			int s = 1;
			for(std::vector<float>::iterator it=monoFrac.begin(); it!=monoFrac.end(); ++it, ++s){
				allSfs[s] = new SFS(&origSfs, *it);
				logfile->flush(sep + toString(*it));
				sep = ", ";
			}
			logfile->newLine();
		}
	}

	//limit sites
	bool limitSites = false;
	long maxNumSites = 0;
	if(params.parameterExists("limitSites")){
		limitSites = true;
		maxNumSites = params.getParameterLong("limitSites");
		if(maxNumSites <=0)
			throw "Can not limit to less than one site!";
	}


	//prepare parsing
	double* LL = new double[numSfs];
	int s;
	for(s=0; s<numSfs; ++s)
		LL[s] = 0.0;
	long numLines = 0;
	std::vector<SFS>::iterator sfsIt;
	struct timeval start, end;
	gettimeofday(&start, NULL);

	//now parse SAF file to calc LL
	logfile->startIndent("Will parse SAF file to calculate LL:");
	while(saf.readLine()){
		if(numSfs == 1){
			LL[0] += origSfs.calcLLOneSite(saf.likelihoods);
		} else {
			for(s=0; s<numSfs; ++s)
				LL[s] += allSfs[s]->calcLLOneSite(saf.likelihoods);
		}

		//progress
		++numLines;
		if(numLines % 1000000 == 0){
			gettimeofday(&end, NULL);
			float runtime=(end.tv_sec  - start.tv_sec)/60.0;
			logfile->list("Parsed " + toString(numLines) + " sites in " + toString(runtime) + " min ...");
		}

		//do we limit sites
		if(limitSites && numLines >= maxNumSites)
			break;
	}

	//report progress at end
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile->list("Parsed " + toString(numLines) + " sites in " + toString(runtime) + " min ...");
	if(!limitSites || numLines < maxNumSites)
		logfile->list("Reached end of SAf file");
	logfile->endIndent();

	//report LL
	if(numSfs == 1){
		logfile->list("Log likelihood was LL = " + toString(LL[0]));
	} else {
		std::string filename = outputName + "_LL.txt";
		std::ofstream out(filename.c_str());
		if(!out)
			throw "Failed to open output file '" + filename + "'!";
		out << "monoFrac\tLL\n";
		logfile->startIndent("Resulting log likelihood value(s):");
		for(s=0; s<numSfs; ++s){
			logfile->list("At " + toString(allSfs[s]->monoFrac) + " monomorphic sites, LL = " + toString(LL[s]));
			out << allSfs[s]->monoFrac << "\t" << LL[s] << "\n";
		}
		logfile->endIndent();
		out.close();
	}
}


//---------------------------------------------------------------------------
//Main function
//---------------------------------------------------------------------------
int main(int argc, char* argv[]){
	struct timeval start, end;
    gettimeofday(&start, NULL);

	TLog logfile;
	logfile.newLine();
	logfile.write(" poolStat 1.0 ");
	logfile.write("**************");
    try{
		//read parameters from the command line
    	TParameters myParameters(argc, argv, &logfile);

		//verbose?
		bool verbose = myParameters.parameterExists("verbose");
		if(!verbose) logfile.listNoFile("Running in silent mode (use 'verbose' to get a status report on screen)");
		logfile.setVerbose(verbose);
		//warnings?
		bool suppressWarnings=myParameters.parameterExists("suppressWarnings");
		if(suppressWarnings){
			logfile.list("Suppressing Warnings");
			logfile.suppressWarings();
		}


		//open log file that handles the output
		std::string  logFilename=myParameters.getParameterString("logFile", false);
		if(logFilename.length()>0){
			logfile.openFile(logFilename.c_str());
			logfile.writeFileOnly(" poolStat 1.0 ");
			logfile.writeFileOnly("**************");
		}

		//what to do?
		std::string task = myParameters.getParameterStringWithDefault("task", "poolStat");
		if(task == "AFL"){
			TGenome genome(&logfile, myParameters);
			logfile.startIndent("Estimating Allele Frequency Likelihoods (AFL):");
			genome.calculateAlleleFrequencyLikelihoods(myParameters);
		} else if(task == "pileup"){
			TGenome genome(&logfile, myParameters);
			logfile.startIndent("Compiling pileup:");
			genome.printPileup(myParameters);
		} else if(task == "approxWF"){
			TGenome genome(&logfile, myParameters);
			logfile.startIndent("Preparing approxWF input from pooled data:");
			genome.generateApproxWFInput(myParameters);
		} else if(task == "findMonomorphicSites"){
			TGenome genome(&logfile, myParameters);
			logfile.startIndent("Identifying monomorphic sites:");
			genome.findStrictlyMonomoprhicSites(myParameters);
		} else if(task == "poolStat"){
			TGenome genome(&logfile, myParameters);
			logfile.startIndent("Estimating allele frequencies and calculating statistics from pooled data:");
			logfile.list("This option assumes that the sample size is unknown. If it is known, use task AFL instead.");
			genome.poolStat(myParameters);
		} else if(task == "sfsLL"){
			logfile.startIndent("Calculating the log-likelihood of an SFS:");
			calcSfsLL(&logfile, myParameters);
		} else if(task == "printSaf"){
			logfile.startIndent("Printing SAF file:");
			printSafFile(&logfile, myParameters);
		} else if(task == "estimateSFS"){
			logfile.startIndent("Estimating SFS (crude):");
			estimateSFS(&logfile, myParameters);
    	} else if(task == "simulate"){
    		logfile.startIndent("Simulating pooled data:");
    		simulatePooledData(&logfile, myParameters);
    	} else throw "Unknown task '" + task + "'!";
		logfile.clearIndent();

		//write unsused parameters
		std::string unusedParams=myParameters.getListOfUnusedParameters();
		if(unusedParams!=""){
			logfile.newLine();
			logfile.warning("The following parameters were not used: " + unusedParams + "!");
		}
    }
	catch (std::string & error){
		logfile.error(error);
	}
	catch (const char* error){
		logfile.error(error);
	}
	catch(std::exception & error){
		logfile.error(error.what());
	}
	catch (...){
		logfile.error("unhandeled error!");
	}
	logfile.clearIndent();
	gettimeofday(&end, NULL);
	float runtime=(end.tv_sec  - start.tv_sec)/60.0;
	logfile.list("Program terminated in ", runtime, " min!");
	logfile.close();
    return 0;
}

