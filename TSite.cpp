/*
 * TBase.cpp
 *
 *  Created on: May 9, 2015
 *      Author: wegmannd
 */

#include "TSite.h"

//-------------------------------------------------------
//TSite
//-------------------------------------------------------
void TSite::clear(){
	if(hasData){
		for(std::vector<TBase*>::iterator it = bases.begin(); it!=bases.end(); ++it)
			delete *it;
		bases.clear();
		hasData = false;
	}
};

void TSite::stealFromOther(TSite* other){
	//this function extracts all data from the other object and sets it to empty
	hasData = other->hasData;
	if(hasData){
		//copy data
		referenceBase = other->referenceBase;
		for(int i=0; i<numGenotypes; ++i){
			emissionProbabilities[i] = other->emissionProbabilities[i];
		}
		//copy pointers to bases, BUT NOT BASES
		for(std::vector<TBase*>::iterator it = other->bases.begin(); it!=other->bases.end(); ++it){
			bases.push_back(*it);
		}
		//remove pointers from other site
		other->bases.clear();
		other->hasData = false;
	}
}

void TSite::add(char & base, char & quality, int & maxNumBasesPerSite){
	if(bases.size() < maxNumBasesPerSite){
		if(base == 'A') bases.push_back(new TBaseHaploidA(quality));
		else if(base == 'C') bases.push_back(new TBaseHaploidC(quality));
		else if(base == 'G') bases.push_back(new TBaseHaploidG(quality));
		else bases.push_back(new TBaseHaploidT(quality));
		hasData = true;
	}
};

void TSite::addToBaseFrequencies(TBaseFrequencies & frequencies){
	double weight = 1.0 / bases.size();
	for(std::vector<TBase*>::iterator it = bases.begin(); it!=bases.end(); ++it){
		(*it)->addToBaseFrequencies(frequencies, weight);
	}
}

void TSite::fillBaseCounts(int* counts){
	for(int i=0; i<5; ++i)
		counts[i] = 0;
	for(std::vector<TBase*>::iterator it = bases.begin(); it!=bases.end(); ++it){
		++counts[(*it)->getBaseAsEnum()];
	}
}

void TSite::calcEmissionProbabilities(){
	//assumes that emission probabilities were calculated for TBase!

	//do in log if coverage is high
	if(bases.size() < 50){
		for(int i=0; i<numGenotypes; ++i){
			emissionProbabilities[i] = 1.0;
		}
		for(std::vector<TBase*>::iterator it = bases.begin(); it!=bases.end(); ++it){
			for(int i=0; i<numGenotypes; ++i){
				emissionProbabilities[i] *= (*it)->getEmissionProbability(i);
			}
		}
	} else {
		for(int i=0; i<numGenotypes; ++i){
			emissionProbabilities[i] = 0.0;
		}
		for(std::vector<TBase*>::iterator it = bases.begin(); it!=bases.end(); ++it){
			for(int i=0; i<numGenotypes; ++i){
				emissionProbabilities[i] += log((*it)->getEmissionProbability(i));
			}
		}
		//now standardize before delog
		double max = emissionProbabilities[0];
		for(int i=1; i<numGenotypes; ++i){
			if(emissionProbabilities[i] > max) max = emissionProbabilities[i];
		}
		for(int i=0; i<numGenotypes; ++i){
			emissionProbabilities[i] = exp(emissionProbabilities[i] - max);
		}
	}
}

std::string TSite::getBases(){
	if(!hasData) return "-";
	std::string b = "";
	for(std::vector<TBase*>::iterator it = bases.begin(); it!=bases.end(); ++it){
		b += (*it)->getBase();
	}
	return b;
}

std::string TSite::getEmissionProbs(){
	std::string b = toString(emissionProbabilities[0]);
	for(int i=1; i<numGenotypes; ++i){
		b += "\t" + toString(emissionProbabilities[i]);
	}
	return b;
}

double TSite::calculateWeightedSumOfEmissionProbs(double* weights){
	//calculate normalized genotype probabilities according to Bayes rule
	double sum = 0.0;
	for(int i=0; i<numGenotypes; ++i){
		sum += emissionProbabilities[i] * weights[i];
	}
	return sum;
}

float TSite::calculateLogLikelihood(float* genotypeProbabilities){
	//calculate normalized genotype probabilities according to Bayes rule
	double sum = 0.0;
	for(int i=0; i<numGenotypes; ++i){
		sum +=  emissionProbabilities[i] * genotypeProbabilities[i];
	}
	return log(sum);
}

void TSite::addToExpectedBaseCounts(TBaseFrequencies & baseFreq, double* expectedCounts){
	double* tmp = new double[4];
	for(int b=0; b<4; ++b) tmp[b]=0.0;
	for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
		(*it)->addToExpectedBaseCounts(baseFreq, tmp);
	}
	for(int b=0; b<4; ++b) expectedCounts[b] += tmp[b] / (double) bases.size();
}

/*
void TSite::calculateAlleleFrequencyLikelihoods(int & numChromosomes, Base & allele1, Base & allele2, float* storage, TGenotypeMap & genoMap){
	if(hasData){
		//calculate likelihood for sample frequencies from 0 to num chromosomes for allele 1
		double LL;
		double f;
		for(int y = 0; y < (numChromosomes + 1); ++y){
			//calculate likelihood
			LL = 0.0;
			f = (double) y / (double) numChromosomes;
			for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
				LL += log((*it)->getEmissionProbability(allele1) * f + (*it)->getEmissionProbability(allele2) * (1.0 - f));
			}
			storage[y] = LL;
		}
	} else {
		for(int y = 0; y < (numChromosomes + 1); ++y)
			storage[y] = 0.0;
	}
}
*/

void TSite::calculateAlleleFrequencyLikelihoods(int & numChromosomes, Base & allele1, Base & allele2, float* storage, TGenotypeMap & genoMap){
	if(hasData){
		//calculate likelihood for sample frequencies from 0 to num chromosomes for allele 1
		double L_tmp;
		double f;
		for(int y = 0; y < (numChromosomes + 1); ++y){
			//calculate likelihood
			//NOTE: removed calculating in log for speed. Seems to work up to 100 coverage
			L_tmp = 1.0;
			f = (double) y / (double) numChromosomes;
			for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
				L_tmp *= (*it)->getEmissionProbability(allele1) * f + (*it)->getEmissionProbability(allele2) * (1.0 - f);
			}
			storage[y] = log(L_tmp);
		}
	} else {
		for(int y = 0; y < (numChromosomes + 1); ++y)
			storage[y] = 0.0;
	}
}

double TSite::getRoughEstimateOfAlleleFrequency(Base & allele1, Base & allele2){
	double f = 0.0;
	double num = 0.0;
	for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
		if((*it)->getBaseAsEnum() == allele1){
			f = f + 1.0;
			num = num + 1.0;
		} else if((*it)->getBaseAsEnum() == allele2)
			num = num + 1.0;
	}
	return f / num;
}

float TSite::calculatePooledLogLikelihood(double & f, Base & allele1, Base & allele2){
	float LL = 0.0;
	for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
		if((*it)->getBaseAsEnum() == allele1)
			LL += log(f*(1.0 - 4.0 * (*it)->errorRateThird) + (*it)->errorRateThird);
		else if((*it)->getBaseAsEnum() == allele2)
			LL += log(f*(4.0 * (*it)->errorRateThird - 1.0) + (1.0 - (*it)->errorRate));
		else LL += log((*it)->errorRateThird);
	}
	return LL;
}

void TSite::calculatePooledLogLikelihoodDerivatives(double & f, Base & allele1, Base & allele2, double & firstDeriv, double & secondDeriv){
	firstDeriv = 0.0;
	secondDeriv = 0.0;
	double v;
	for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
		if((*it)->getBaseAsEnum() == allele1){
			v = (3.0 - 4.0 * (*it)->errorRate);
			v =  v / (f * v + (*it)->errorRate);
			firstDeriv += v;
			secondDeriv += v*v;
		} else if((*it)->getBaseAsEnum() == allele2){
			v = (3.0 - 4.0 * (*it)->errorRate);
			v =  v / (f * v + 3.0 * (*it)->errorRate - 3.0);
			firstDeriv += v;
			secondDeriv -= v*v;
		}
	}
}


float TSite::calculatePooledLogLikelihoodNullModel(Base allele){
	float LL = 0.0;
	for(std::vector<TBase*>::iterator it = bases.begin(); it != bases.end(); ++it){
		if((*it)->getBaseAsEnum() == allele)
			LL += log(1.0 - (*it)->errorRateThird);
		else LL += log((*it)->errorRateThird);
	}
	return LL;
}


















