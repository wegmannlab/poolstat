/*
 * TBamFile.h
 *
 *  Created on: Nov 18, 2016
 *      Author: phaentu
 */

#ifndef TBAMFILE_H_
#define TBAMFILE_H_

#include "TWindow.h"
#include "gzstream.h"
#include "bamtools/api/BamWriter.h"
#include "TLog.h"
#include "TSafFile.h"
#include "TBase.h"
#include <typeinfo>
#include <map>


//---------------------------------------------------------------
//TReferenceAndMask
//---------------------------------------------------------------
class TReferenceAndMask{
public:
	TBedReader* mask;
	bool doMasking;
	bool doCpGMasking;
	bool applyCoverageFilter, applyQualityFilter;
	int minCoverage, maxCoverage;
	int minQuality, maxQuality;
	int maxNumBasesReadPerSite;
	BamTools::Fasta reference;
	std::string referenceForWindow;
	bool fastaReference;

	TReferenceAndMask(TLog* Logfile, TParameters & params, int windowSize);
	~TReferenceAndMask(){
		if(doMasking) delete mask;
		if(fastaReference) reference.Close();
	};
	void setChr(std::string & chr);
	void fillReference(int refID, int windowStart, int windowEnd);
};

//---------------------------------------------------------------
//TBamFile
//---------------------------------------------------------------
class TBamFile{
public:
	std::string filename;
	std::string filenameTag;
	TLog* logfile;
	BamTools::BamReader bamReader;
	BamTools::SamHeader bamHeader;
	TReadGroups readGroups;
	BamTools::BamAlignment bamAlignment;
	bool oldAlignementMustBeConsidered;
	TReferenceAndMask* refAndMask;
	int numChromosomes;
	bool numchromosomesKnown;

	TWindowPair windowPair;
	long oldPos;

	BamTools::SamSequenceIterator chrIterator;
	int chrNumber;
	long chrLength;
 	long curStart;
 	long curEnd;
 	int curWindowSize;
 	int windowSize;
 	int numWindowsOnChr;
 	int windowNumber;

 	TSafFile* safFile;
 	bool safFileOpend;

	TBamFile(){
		oldAlignementMustBeConsidered = false;
		oldPos = 0;
		chrNumber = 0;
		chrLength = 0;
	 	curStart = 0;
	 	curEnd = 0;
	 	curWindowSize = 0;
	 	windowSize = 0;
	 	numWindowsOnChr = 0;
	 	windowNumber = 0;
	 	refAndMask = NULL;
	 	numChromosomes = -1;
	 	numchromosomesKnown = false;
	 	logfile = NULL;
	 	safFile = NULL;
	 	safFileOpend = false;
	};
	~TBamFile(){
		if(safFileOpend) delete safFile;
	};
	TBamFile(std::string Filename, const int & WindowSize, TReferenceAndMask* RefAndMask, TLog* Logfile);
	void openBamFile(std::string Filename, const int & WindowSize, TReferenceAndMask* RefAndMask, TLog* Logfile);
	void setNumChromosomes(int NumChromosomes);

	void jumpToEnd();
	bool setChr(std::string & name);
	void fillVectorOfChromosomes(std::vector<std::string> & vec);
	long iterateWindow();
	bool addAlignementToWindows(BamTools::BamAlignment & alignement);
	bool readData();
	long getCurPos(int & posInWindow);
	void printPileup(std::ofstream & out, int & posInWindow);
	void fillBaseCounts(int** counts);
	bool siteHasData(int & site){return windowPair.curPointer->siteHasData(site);};

	//AFL
	void calcAlleleFrequencyLikelihoods();
	void addToAlleleFrequencyLikelihoodAtMLE(float** alleleFrequencyLikelihoodAtMLE, float** alleleFrequencyLikelihoodAtMonomorphic);
	void writeAlleleFrequencyLikelihoodHeader(gz::ogzstream & outFile, std::string name);
	void writeAlleleFrequencyLikelihoods(gz::ogzstream & outFile, int site, int alleleicCombination);
	void writeMLEAlleleFrequency(gz::ogzstream & outFile, int site, int alleleicCombination);
	void writeMLEAlleleFrequencyFlip(gz::ogzstream & outFile, int site, int alleleicCombination);
	double getMLEAlleleFrequency(const int & site, const int & alleleicCombination);
	double getMLEAlleleFrequencyFlip(const int & site, const int & alleleicCombination);
	int getMLEAlleleCounts(const int & site, const int & alleleicCombination);
	void addToPooledMLEAlleleFrequency(int & site, int & alleleicCombination, float & freq, float & nChr);
	void openSafFile();
	void setSmallestAFLToPrint(float value);
	void writeAlleleFrequencyLikelihoodsToSafFile(std::string & chr, int site, int alleleicCombination);
	void writeAlleleFrequencyLikelihoodsToSafFileFlipOrder(std::string & chr, int site, int alleleicCombination);
	void writeMLEAlleleCountToApproxWFFile(gz::ogzstream & out, const int & site, const int & alleleicCombination);

	//pool of unknown sample size
	void findMLEFrequenciesPooled(int & maxNumIterations, float & maxDeltaF);
	void addToPooledFrequencyLikelihoodAtMLE(float** likelihoodAtMLE, float** likelihoodAtMonomorphic);
	float getMLEAlleleFrequencyPooled(int site, int alleleicCombination);
};




#endif /* TBAMFILE_H_ */
