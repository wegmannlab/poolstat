/*
 * safFileFunctions.h
 *
 *  Created on: Nov 15, 2016
 *      Author: phaentu
 *
 *  Most of the code was directly taken from ANGSD with slight modifications
 */

#ifndef TSAFFILE_H_
#define TSAFFILE_H_

#include <htslib/bgzf.h>
//#include <htslib/hts.h>
//#include <htslib/sam.h>
#include <string>
#include <iostream>
#include <math.h>

#include "TLog.h"
#include "stringFunctions.h"

#define GZOPT "w6"

//--------------------------------
//BGZF class
//--------------------------------
class TBGZF_file{
private:
	std::string filename;

	int64_t offset;
	bool isOpen;

public:

	BGZF* filePointer;

	TBGZF_file(){
		filename = "";
		filePointer = NULL;
		isOpen = false;
		offset = 0;
	};

	TBGZF_file(std::string Filename):TBGZF_file(){
		open(Filename);
	};

	~TBGZF_file(){
		close();
	};

	void open(std::string filename){
		if(isOpen)
			throw "BGZF file already open!";

		filePointer = bgzf_open(filename.c_str(),GZOPT);
		if(filePointer == NULL)
			throw "Problem opening file '" + filename + "'! Check permissions?";
		offset = 0;
		isOpen = true;
	};

	void close(){
		if(isOpen)
			bgzf_close(filePointer);
		isOpen = false;
	};

	ssize_t write(const void *data, size_t length){
		if(length % sizeof(float) != 0)
			throw "length is not a multiple of the size of floats!";

		if(length>0){
			int tmp = bgzf_write(filePointer,data,length);
	        offset += tmp;
			//offset += length;

	        //std::cout << "offset = " << offset << " = " << bgzf_tell(filePointer) << std::endl;

	        return tmp;
		}
	    return 0;
	};

	int64_t getCurrentOffset(){
		return offset;
	};
};

//--------------------------------
//SAF file class
//--------------------------------
class TSafFile{
private:
	TBGZF_file outfileSAF;
	TBGZF_file outfileSAFPOS;
	FILE* outfileSAFIDX;
	bool filesOpend;
	int64_t offsetSAF;
	int64_t offsetSAFPOS;
	int numChromosomes;
	int dimension;
	std::string chrName;
	int numSitesWrittenOnChr;
	float smallestAFLPrinted;
	bool truncateSmallAFLValues;

	FILE *openFile(std::string filename);
	//BGZF *openFileBG(std::string filename);
	//ssize_t write_to_bgzf(BGZF *fp, const void *data, size_t length);
	void storeCurrentOffset();
	void updateIndex();

public:
	TSafFile(std::string & outputName, int NumChromosomes);
	~TSafFile();

	void open(std::string & outputName);
	void close();

	void setSmallestAFLToPrint(float value);

	void writeLogAFLs(std::string & chr, int pos, float* logAFLs);


};


//--------------------------------
//Class to read a SAf file
//--------------------------------
class TSafFileReader{
private:
	std::string filename;
	std::string filenamePos;
	std::string filenameIdx;
	int numChromosomes;
	int dimension;
	size_t sizeOfChunkToRead;
	bool fileIsOpen;
	BGZF* inputSafFile;
	BGZF* inputSafPosFile;
	FILE* inputSafIdxFile;
	std::string safVersion;
	int64_t posOnChr;
	int64_t posRead;
	int64_t offsetSAF;
	int64_t offsetSAFPOS;

	bool readChromosomeInfo();

public:
	float* likelihoods;
	int position;
	std::string chromosome;

	TSafFileReader(std::string Filename);
	~TSafFileReader();

	void open(std::string & Filename);
	std::string version(){return safVersion;};
	void close();
	int sampleSize(){return numChromosomes;};
	int dimensionality(){return dimension;};
	bool readLine();
	void printLine();
};

#endif /* TSAFFILE_H_ */
