/*
 * TBase.h
 *
 *  Created on: May 9, 2015
 *      Author: wegmannd
 */

#ifndef TSITE_H_
#define TSITE_H_

#include <math.h>
#include <vector>
#include "TParameters.h"
#include "TGenotypeMap.h"
#include "TBase.h"
#include "gzstream.h"
#include <algorithm>
#include "TRandomGenerator.h"

//---------------------------------------------------------------
//TSite
//---------------------------------------------------------------
class TSite{
public:
	bool hasData;
	std::vector<TBase*> bases;
	int numGenotypes;
	double* emissionProbabilities;
	char referenceBase; //optional
	double maxQualToPrint, maxQualToPrintNaturalScale;

	TSite(){
		hasData = false;
		numGenotypes = 0;
		emissionProbabilities = NULL;
		referenceBase = 'N';
		maxQualToPrint = 1000;
		maxQualToPrintNaturalScale = pow(10.0, -maxQualToPrint / 10.0);
		emissionProbabilities = new double[numGenotypes];
	};
	~TSite(){
		clear();
		delete[] emissionProbabilities;
	};
	void clear();
	void stealFromOther(TSite* other);

	void add(char & base, char & quality, int & maxNumBasesPerSite);
	void setRefBase(char & Base){
		if(Base == 'A' || Base == 'C' || Base == 'G' || Base == 'T')
			referenceBase = Base;
		else referenceBase = 'N';
	};
	char getRefBase(){return referenceBase;};
	Base getRefBaseAsEnum(){
		if(referenceBase == 'A') return A;
		if(referenceBase == 'C') return C;
		if(referenceBase == 'G') return G;
		if(referenceBase == 'T') return T;
		return N;
	};
	int getCoverage(){return bases.size();};
	void addToBaseFrequencies(TBaseFrequencies & frequencies);
	void fillBaseCounts(int* counts);
	double makePhred(double epsilon){
		return makePhredByRef(epsilon);
	};
	double makePhredByRef(double & epsilon){
		if(epsilon < maxQualToPrintNaturalScale) return maxQualToPrint;
		return -10.0 * log10(epsilon);
	};
	void calcEmissionProbabilities();
	double calculateWeightedSumOfEmissionProbs(double* weights);
	std::string getBases();
	std::string getEmissionProbs();

	//AFL
	float calculateLogLikelihood(float* genotypeProbabilities);
	void addToExpectedBaseCounts(TBaseFrequencies & baseFreq, double* expectedCounts);
	void calculateAlleleFrequencyLikelihoods(int & numChromosomes, Base & allele1, Base & allele2, float* storage, TGenotypeMap & genoMap);
	double getRoughEstimateOfAlleleFrequency(Base & allele1, Base & allele2);

	//pooled
	float calculatePooledLogLikelihood(double & f, Base & allele1, Base & allele2);
	void calculatePooledLogLikelihoodDerivatives(double & f, Base & allele1, Base & allele2, double & firstDeriv, double & secondDeriv);
	float calculatePooledLogLikelihoodNullModel(Base allele);
};

#endif /* TSITE_H_ */
