/*
 * loci.h
 *
 *  Created on: Feb 19, 2015
 *      Author: wegmannd
 */

#ifndef LOCI_H_
#define LOCI_H_

#include "TBamFile.h"

//---------------------------------------------------------------
//TGenome
//---------------------------------------------------------------
class TGenome{
private:
	void jumpToEnd();
	void restartChromosome();
	bool iterateChromosome();
	void moveBamFilesToChromosome(std::string & chr);
	bool iterateWindow();
	bool readData();
	void initializePostMortemDamage(TParameters & params);
	void initializeRecalibration(TParameters & params);
	void openThetaOutputFile(std::ofstream & out);
	void initializeRandomGenerator(TParameters & params);
	float getLog10Pvalue_LRT(float & D);

	void parseTimePoints(std::string timePointString, std::vector< std::vector<int> > & timePointVec, int & totTimePoints);

	TRandomGenerator* randomGenerator;
	TLog* logfile;
	bool randomGeneratorInitialized;
	std::string outputName;

	TBamFile* bamFiles;
	int numBamFiles;
	bool sampleSizeKnown;
	long windowSize;
	int windowNumber;
	TReferenceAndMask* refAndMask;
	TGenotypeMap genoMap;

 	std::vector<std::string> chromosomes;
 	std::vector<std::string>::iterator chrIt;
 	int chrNumber;
 	long chrLength;
 	int numWindowsOnChr;

 	long limitWindows;
 	int limitChr;
 	bool* useChromosome;

 	float chisq_distance_max = 69.07915;
 	float chisq_log10_pvalue_max = -15.0;

public:
	TGenome(TLog* Logfile, TParameters & params);
	~TGenome(){
		if(randomGeneratorInitialized) delete randomGenerator;
		if(useChromosome) delete[] useChromosome;
		delete[] bamFiles;
		delete refAndMask;
	};

	bool openFastaReferenceForCaller(TParameters & params, BamTools::Fasta & reference);
	void printPileup(TParameters & params);
	void generateApproxWFInput(TParameters & params);
	void findStrictlyMonomoprhicSites(TParameters & params);

	void testSafFile(TParameters & params);

	void calculateAlleleFrequencyLikelihoods(TParameters & params);
	void poolStat(TParameters & params);
};





#endif /* LOCI_H_ */
